import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SuratController extends GetxController {
  var dataSurat = [].obs;
  var loading = false.obs;
  var search = false.obs;
  var searchController = TextEditingController(text: '').obs;
  //TODO: Implement HomeController

  final count = 0.obs;
  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    await getDataSurat();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  onChange(value) {
    if (value != "") {
      search.value = true;
    } else {
      search.value = false;
    }
  }

  getDataSurat() async {
    loading.value = true;
    dataSurat.clear();
    var data = await ApiHelper().getDataSurat(searchController.value.text);
    print(data);
    if (data['status'] == 'OK') {
      for (var x = 0; x < data['data'].length; x++) {
        var detail = {
          'judul': data['data'][x]['KETERANGAN'],
          'link': data['data'][x]['NAMAFILE'],
        };
        dataSurat.add(detail);
      }
    }
    dataSurat.refresh();
    loading.value = false;
  }
}
