import 'package:alpha_bpd_apk/app/modules/surat/controllers/surat_controller.dart';
import 'package:get/get.dart';

class SuratBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SuratController());
  }
}
