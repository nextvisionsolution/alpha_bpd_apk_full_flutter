import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/modules/surat/controllers/surat_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SuratView extends GetView<SuratController> {
  const SuratView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
        child: Container(
            height: GlobalVariable.ratioWidth(Get.context!) * 56,
            decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1))),
            padding: EdgeInsets.symmetric(
              vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
              horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
            ),
            child: Row(
              children: [
                GestureDetector(
                  child: SvgPicture.asset("assets/icon/chevron_left.svg", width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 30),
                  onTap: () {
                    Get.back();
                  },
                ),
                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                Expanded(
                  child: Text(
                    LocaleLanguage().tr(LocaleKeys.SURAT),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large),
                  ),
                )
              ],
            )),
      ),
      body: Column(
        children: [
          SizedBox(
            height: GlobalVariable.ratioWidth(Get.context!) * 14,
          ),
          Container(
              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
              child: TextField(
                controller: controller.searchController.value,
                style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                onChanged: (value) {
                  controller.onChange(value);
                },
                onEditingComplete: () async {
                  await controller.getDataSurat();
                },
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(right: GlobalVariable.ratioWidth(Get.context!) * 8, top: GlobalVariable.ratioWidth(Get.context!) * 12, bottom: GlobalVariable.ratioWidth(Get.context!) * 12),
                    hintText: LocaleLanguage().tr(LocaleKeys.CARISURAT),
                    hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                    ),
                    prefixIcon: Container(
                        margin: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 8, right: GlobalVariable.ratioWidth(Get.context!) * 10),
                        child: SvgPicture.asset(
                          'assets/icon/magnify.svg',
                          color: AppColors.goldColor,
                        )),
                    suffixIcon: Obx(() => controller.search.value
                        ? GestureDetector(
                            onTap: () {
                              controller.onChange("");
                              controller.searchController.value.clear();
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 8, right: GlobalVariable.ratioWidth(Get.context!) * 10),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.black,
                                  size: GlobalVariable.ratioWidth(Get.context!) * 20,
                                )))
                        : SizedBox()),
                    // Set border for focused state
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                    )),
              )),
          SizedBox(
            height: GlobalVariable.ratioWidth(Get.context!) * 14,
          ),
          Obx(() => controller.loading.value
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : controller.dataSurat.length == 0
                  ? Expanded(
                      child: Center(
                          child: Text(
                      LocaleLanguage().tr(LocaleKeys.TIDAKADADATA),
                      style: TextStyle(fontSize: AppFontSize.large),
                    )))
                  : SingleChildScrollView(
                      child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        for (var x = 0; x < controller.dataSurat.length; x++)
                          Container(
                              margin: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16, bottom: GlobalVariable.ratioWidth(Get.context!) * 10),
                              padding: EdgeInsets.all(GlobalVariable.ratioWidth(Get.context!) * 10),
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10), border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                              child: Row(
                                children: [
                                  GestureDetector(
                                      onTap: () async {
                                        if (!await launchUrl(Uri.parse(controller.dataSurat[x]['link']))) {
                                          throw Exception('Could not launch');
                                        }
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(GlobalVariable.ratioWidth(Get.context!) * 5),
                                          child: SvgPicture.asset(
                                            'assets/icon/download.svg',
                                            width: GlobalVariable.ratioWidth(Get.context!) * 24,
                                            height: GlobalVariable.ratioWidth(Get.context!) * 24,
                                            color: AppColors.goldColor,
                                          ))),
                                  SizedBox(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 6,
                                  ),
                                  Expanded(
                                      child: Text(
                                    controller.dataSurat[x]['judul'],
                                    style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  )),
                                ],
                              ))
                      ],
                    ))),
        ],
      ),
    ));
  }
}
