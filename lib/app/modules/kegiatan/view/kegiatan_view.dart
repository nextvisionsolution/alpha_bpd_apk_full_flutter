import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/kegiatan/controllers/kegiatan_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:url_launcher/url_launcher.dart';

class KegiatanView extends GetView<KegiatanController> {
  const KegiatanView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.backgroundKegiatanColor,
            body: Obx(() => controller.loading.value
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(
                    children: [
                      Positioned(
                          child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Column(children: [
                                Expanded(
                                    child: PhotoViewGallery.builder(
                                  scrollPhysics: const BouncingScrollPhysics(),
                                  backgroundDecoration: BoxDecoration(color: AppColors.backgroundKegiatanColor),
                                  builder: (BuildContext context, int index) {
                                    return PhotoViewGalleryPageOptions(
                                      imageProvider: NetworkImage(controller.arrGambar[index]),
                                      heroAttributes: PhotoViewHeroAttributes(tag: index),
                                    );
                                  },
                                  itemCount: controller.arrGambar.length,
                                  loadingBuilder: (context, event) => Center(
                                    child: Container(
                                      width: 20.0,
                                      height: 20.0,
                                      child: CircularProgressIndicator(),
                                    ),
                                  ),
                                )),
                                Container(
                                  width: MediaQuery.of(Get.context!).size.width,
                                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16), topRight: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16))),
                                  padding: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 16, bottom: GlobalVariable.ratioWidth(Get.context!) * 16, left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        controller.dataKegiatan['NAMAKEGIATAN'],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset('assets/icon/calendar.svg', width: GlobalVariable.ratioFontSize(Get.context!) * 14, height: GlobalVariable.ratioFontSize(Get.context!) * 14),
                                          SizedBox(
                                            width: GlobalVariable.ratioFontSize(Get.context!) * 4,
                                          ),
                                          Text(
                                            GlobalVariable.formatDate(controller.dataKegiatan['TGLKEGIATAN']),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset('assets/icon/clock.svg', width: GlobalVariable.ratioFontSize(Get.context!) * 14, height: GlobalVariable.ratioFontSize(Get.context!) * 14),
                                          SizedBox(
                                            width: GlobalVariable.ratioFontSize(Get.context!) * 4,
                                          ),
                                          Text(
                                            GlobalVariable.formatTime(controller.dataKegiatan['JAMKEGIATAN']),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset('assets/icon/whatsapp.svg', width: GlobalVariable.ratioFontSize(Get.context!) * 14, height: GlobalVariable.ratioFontSize(Get.context!) * 14),
                                          SizedBox(
                                            width: GlobalVariable.ratioFontSize(Get.context!) * 4,
                                          ),
                                          Text(
                                            controller.dataKegiatan['NAMAPENANGGUNGJAWAB'] == "" && controller.dataKegiatan['NAMAPENANGGUNGJAWAB'] == ""
                                                ? "-"
                                                : ((controller.dataKegiatan['NAMAPENANGGUNGJAWAB'] == "" ? "-" : controller.dataKegiatan['NAMAPENANGGUNGJAWAB']) + " / " + (controller.dataKegiatan['HPPENANGGUNGJAWAB'] == "" ? "-" : controller.dataKegiatan['HPPENANGGUNGJAWAB'])),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                      controller.jenisKegiatan == "INCOMING"
                                          ? Container(
                                              width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                              child: ElevatedButton(
                                                  onPressed: () {
                                                    controller.popUpDaftarUntuk(controller.idKegiatan);
                                                  },
                                                  child: Text(
                                                    LocaleLanguage().tr(LocaleKeys.DAFTAR),
                                                    style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                                                  ),
                                                  style: ButtonStyle(
                                                      elevation: MaterialStateProperty.all<double>(0),
                                                      padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 12)),
                                                      backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor))))))
                                          : Container(
                                              width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                              child: ElevatedButton(
                                                  onPressed: () async {
                                                    if (!await launchUrl(Uri.parse(controller.dataKegiatan['LAPORAN']))) {
                                                      throw Exception('Could not launch');
                                                    }
                                                  },
                                                  child: Text(
                                                    LocaleLanguage().tr(LocaleKeys.LAPORAN),
                                                    style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                                  ),
                                                  style: ButtonStyle(
                                                      elevation: MaterialStateProperty.all<double>(0),
                                                      padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                    ],
                                  ),
                                )
                              ]))),
                      Positioned(
                          top: GlobalVariable.ratioWidth(Get.context!) * 16,
                          left: GlobalVariable.ratioWidth(Get.context!) * 16,
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: GestureDetector(
                              child: ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 50)),
                                  child: Container(
                                    color: Colors.white,
                                    child: SvgPicture.asset(
                                      "assets/icon/chevron_left.svg",
                                      width: GlobalVariable.ratioWidth(Get.context!) * 30,
                                      height: GlobalVariable.ratioWidth(Get.context!) * 30,
                                      color: AppColors.backgroundKegiatanColor,
                                    ),
                                  )),
                              onTap: () {
                                Get.back();
                              },
                            ),
                          ))
                    ],
                  ))));
  }
}
