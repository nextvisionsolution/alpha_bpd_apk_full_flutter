import 'package:alpha_bpd_apk/app/modules/kegiatan/controllers/kegiatan_controller.dart';
import 'package:get/get.dart';

class KegiatanBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(KegiatanController());
  }
}
