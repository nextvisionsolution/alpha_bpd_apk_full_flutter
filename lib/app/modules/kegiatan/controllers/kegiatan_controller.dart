import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class KegiatanController extends GetxController {
  //TODO: Implement HomeController
  var arrGambar = [].obs;
  var idKegiatan;
  var jenisKegiatan;
  var dataKegiatan = {}.obs;
  var loading = false.obs;

  var namaController = TextEditingController(text: '').obs;
  var telpController = TextEditingController(text: '').obs;
  var catatanController = TextEditingController(text: '').obs;
  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    jenisKegiatan = Get.arguments[0];
    idKegiatan = Get.arguments[1];

    await getDataKegiatan(idKegiatan);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getDataKegiatan(id) async {
    loading.value = true;
    arrGambar.clear();
    var data = await ApiHelper().getDataKegiatan(id);

    if (data['status'] == 'OK') {
      dataKegiatan.value = data['data'];
      arrGambar.add(dataKegiatan['GAMBAR1']);
      arrGambar.add(dataKegiatan['GAMBAR2']);
    }
    arrGambar.refresh();

    loading.value = false;
  }

  daftarKegiatan(id, nama, telp, catatan, jenis) async {
    GlobalVariable.loadingPopUp(LocaleKeys.LOADING);
    var data = await ApiHelper().daftarKegiatan(id, nama, telp, catatan, jenis);
    print(data);
    Get.back();
    if (data['status'] == 'OK') {
      GlobalVariable.successPopUp("");
    } else {
      GlobalVariable.errorPopUp(LocaleLanguage().tr(LocaleKeys.ERRORDAFTAR));
    }
  }

  popUpDaftarUntuk(id) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(LocaleLanguage().tr(LocaleKeys.DAFTARKAN), textAlign: TextAlign.center, style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(child: Text("")),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                          daftarKegiatan(id, "", "", "", "DIRISENDIRI");
                                        },
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/person.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              LocaleLanguage().tr(LocaleKeys.DIRISENDIRI),
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                          popUpDaftarOrangLain(id);
                                        },
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/group.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              LocaleLanguage().tr(LocaleKeys.ORANGLAIN),
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Expanded(child: Text("")),
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }

  void popUpDaftarOrangLain(id) async {
    namaController.value.clear();
    telpController.value.clear();
    catatanController.value.clear();
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Container(width: GlobalVariable.ratioWidth(Get.context!) * 24, child: Text("")),
                                Expanded(
                                  child: Text(""),
                                ),
                                Text(LocaleLanguage().tr(LocaleKeys.DAFTARKAN) + " " + LocaleLanguage().tr(LocaleKeys.ORANGLAIN), textAlign: TextAlign.center, style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                                Expanded(
                                  child: Text(""),
                                ),
                                GestureDetector(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                        width: GlobalVariable.ratioWidth(Get.context!) * 24,
                                        child: Icon(
                                          Icons.close,
                                          size: GlobalVariable.ratioWidth(Get.context!) * 20,
                                        )))
                              ],
                            ),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Text(LocaleLanguage().tr(LocaleKeys.NAMA), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            TextField(
                              controller: namaController.value,
                              style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                  hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANNAMA),
                                  hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  ),
                                  // Set border for focused state
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  )),
                            ),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Text(LocaleLanguage().tr(LocaleKeys.PHONE), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            TextField(
                              controller: telpController.value,
                              style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                  hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANPHONE),
                                  hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  ),
                                  // Set border for focused state
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  )),
                            ),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Text(LocaleLanguage().tr(LocaleKeys.CATATAN), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            TextField(
                              controller: catatanController.value,
                              maxLines: 5,
                              style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                  hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANCATATAN),
                                  hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  ),
                                  // Set border for focused state
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                  )),
                            ),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.BATAL),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                          daftarKegiatan(id, namaController.value.text, telpController.value.text, catatanController.value.text, "ORANGLAIN");
                                        },
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.DAFTAR),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1))))))
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }
}
