import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var dataLogin;
  var dataBPD = {}.obs;
  var dataPerpuluhanIuran = {}.obs;
  var dataInformasiPejabat = [].obs;
  var dataIncomingEvent = [].obs;
  var dataOutcomingEvent = [].obs;
  var loading = false.obs;
  var loadingPerpuluhanIuran = false.obs;
  var loadingInformasiPejabat = false.obs;
  var loadingIncomingEvent = false.obs;
  var loadingOutcomingEvent = false.obs;
  //TODO: Implement HomeController

  @override
  void onInit() async {
    super.onInit();

    loading.value = true;
    loadingPerpuluhanIuran.value = true;
    loadingInformasiPejabat.value = true;
    loadingIncomingEvent.value = true;
    loadingOutcomingEvent.value = true;

    dataLogin = await SharedPreferencesHelper.getDataLogin();

    GlobalVariable.idPerusahaan = dataLogin['idperusahaan'];
    GlobalVariable.idPejabat = dataLogin['idpejabat'];
    GlobalVariable.idGereja = dataLogin['idgereja'];
    GlobalVariable.kodeGereja = dataLogin['kodegereja'];
    GlobalVariable.statusGembala = (dataLogin['gembala'] == '1' ? true : false);

    await callData();
  }

  callData() async {
    loading.value = true;
    loadingPerpuluhanIuran.value = true;
    loadingInformasiPejabat.value = true;
    loadingIncomingEvent.value = true;
    loadingOutcomingEvent.value = true;

    await initData();
    await perpuluhanIuranData();
    await informasiPejabatData();
    await informasiIncomingEvent();
    await informasiOutcomingEvent();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  initData() async {
    var data = await ApiHelper().getDataPejabat();
    loading.value = false;
    print(data['data'][0]);
    if (data['status'] == 'OK') {
      dataBPD.value = data['data'][0];
    }
  }

  perpuluhanIuranData() async {
    var data = await ApiHelper().getDataPerpuluhanIuranTerakhir();
    loadingPerpuluhanIuran.value = false;
    print(data['data']);
    if (data['status'] == 'OK') {
      dataPerpuluhanIuran.value = data['data'];
    }
  }

  informasiPejabatData() async {
    dataInformasiPejabat.clear();
    var data = await ApiHelper().getDataInformasiPejabat();
    loadingInformasiPejabat.value = false;
    print(data);
    if (data['status'] == 'OK') {
      for (var x = 0; x < data['data'].length; x++) {
        var tgliuran = "-";
        if (data['data'][x]['TGLIURANTERAKHIR'] != "" && data['data'][x]['TGLIURANTERAKHIR'] != null) {
          tgliuran = GlobalVariable.formatDate(data['data'][x]['TGLIURANTERAKHIR']).split(" ")[1] + " " + GlobalVariable.formatDate(data['data'][x]['TGLIURANTERAKHIR']).split(" ")[2];
        }
        var detail = {
          'idpejabat': data['data'][x]['IDPEJABAT'],
          'imagepejabat': data['data'][x]['GAMBARPEJABAT'],
          'namapejabat': data['data'][x]['NAMAJABATAN'] + ". " + data['data'][x]['NAMAPEJABAT'],
          'telppejabat': data['data'][x]['TELP'],
          'tgliuran': tgliuran,
          'amountiuran': data['data'][x]['AMOUNTIURANTERAKHIR'],
        };
        dataInformasiPejabat.add(detail);
      }
    }
    dataInformasiPejabat.refresh();
  }

  informasiIncomingEvent() async {
    dataIncomingEvent.clear();
    var data = await ApiHelper().getDataIncomingEvent();
    loadingIncomingEvent.value = false;
    if (data['status'] == 'OK') {
      for (var x = 0; x < data['data'].length; x++) {
        var detail = {
          'idevent': data['data'][x]['IDKEGIATAN'] ?? "-",
          'imageevent': data['data'][x]['GAMBAR1'] ?? "-",
          'namaevent': data['data'][x]['NAMAKEGIATAN'] ?? "-",
          'tglevent': GlobalVariable.formatDate(data['data'][x]['TGLKEGIATAN']),
          'lokasievent': data['data'][x]['ALAMAT'] ?? "-",
        };
        dataIncomingEvent.add(detail);
      }
    }
    dataIncomingEvent.refresh();
  }

  informasiOutcomingEvent() async {
    dataOutcomingEvent.clear();
    var data = await ApiHelper().getDataOutcomingEvent();
    loadingOutcomingEvent.value = false;
    if (data['status'] == 'OK') {
      for (var x = 0; x < data['data'].length; x++) {
        var detail = {
          'idevent': data['data'][x]['IDKEGIATAN'] ?? "-",
          'imageevent': data['data'][x]['GAMBAR1'] ?? "-",
          'namaevent': data['data'][x]['NAMAKEGIATAN'] ?? "-",
          'tglevent': GlobalVariable.formatDate(data['data'][x]['TGLKEGIATAN']),
          'lokasievent': data['data'][x]['ALAMAT'] ?? "-",
        };
        dataOutcomingEvent.add(detail);
      }
    }
    dataOutcomingEvent.refresh();
  }

  menu(onTap, icon, backIconColor, text, {bool khusus = false}) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
            // width: GlobalVariable.ratioWidth(Get.context!) * 64,
            child: Column(
          children: [
            Container(
                decoration: BoxDecoration(color: backIconColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60)),
                padding: EdgeInsets.all(GlobalVariable.ratioWidth(Get.context!) * 10),
                child: SvgPicture.asset(
                  icon,
                  width: GlobalVariable.ratioWidth(Get.context!) * 30,
                  height: GlobalVariable.ratioWidth(Get.context!) * 30,
                  color: Colors.white,
                )),
            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 2),
            khusus
                ? Container(
                    width: GlobalVariable.ratioWidth(Get.context!) * 50,
                    child: Text(
                      text,
                      style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12, fontWeight: FontWeight.w400),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ))
                : Text(
                    text,
                    style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12, fontWeight: FontWeight.w400),
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
          ],
        )));
  }
}
