import 'package:alpha_bpd_apk/app/modules/bpd/controllers/bpd_controller.dart';
import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/kegiatan/controllers/kegiatan_controller.dart';
import 'package:alpha_bpd_apk/app/modules/materi/controllers/materi_controller.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/binding/perpuluhan_iuran_binding.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/controllers/perpuluhan_iuran_controller.dart';
import 'package:alpha_bpd_apk/app/modules/profil/controllers/profil_controller.dart';
import 'package:alpha_bpd_apk/app/modules/setting/controllers/setting_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/modules/surat/controllers/surat_controller.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          GlobalVariable.out();
          return false;
        },
        child: SafeArea(
            child: Obx(() => controller.loading.value
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Scaffold(
                    backgroundColor: Colors.white,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
                      child: Container(
                          height: GlobalVariable.ratioWidth(Get.context!) * 56,
                          decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1))),
                          padding: EdgeInsets.symmetric(
                            vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
                            horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                          ),
                          child: !controller.loadingPerpuluhanIuran.value
                              ? Row(
                                  children: [
                                    GestureDetector(
                                        onTap: () {
                                          GetToPage.toNamed<BPDController>(Routes.BPD);
                                        },
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                            child: Container(
                                              height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                              width: GlobalVariable.ratioWidth(Get.context!) * 32,
                                              child: CachedNetworkImage(
                                                imageUrl: controller.dataBPD['GAMBARPERUSAHAAN'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                imageBuilder: (context, imageProvider) => Container(
                                                  height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: imageProvider,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 32, width: GlobalVariable.ratioWidth(Get.context!) * 32, child: CircularProgressIndicator()),
                                                errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 32, width: GlobalVariable.ratioWidth(Get.context!) * 32, child: Center(child: Icon(Icons.hide_image_outlined))),
                                              ),
                                            ))),
                                    SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                    Expanded(
                                        child: GestureDetector(
                                      onTap: () {
                                        GetToPage.toNamed<BPDController>(Routes.BPD);
                                      },
                                      child: Text(
                                        "BPD " + controller.dataBPD['NAMAPERUSAHAAN'],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large),
                                      ),
                                    )),
                                    SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                    GestureDetector(
                                      child: SvgPicture.asset("assets/icon/setting.svg", width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 30),
                                      onTap: () {
                                        GetToPage.toNamed<SettingController>(Routes.SETTING);
                                      },
                                    )
                                  ],
                                )
                              : SizedBox()),
                    ),
                    body: SingleChildScrollView(
                        child: Column(
                      children: [
                        Material(
                            elevation: GlobalVariable.ratioWidth(Get.context!) * 6,
                            child: Container(
                                child: Stack(
                              children: [
                                GestureDetector(
                                    onTap: () {},
                                    child: CachedNetworkImage(
                                      imageUrl: controller.dataBPD['GAMBARBANNERGEREJA'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                      imageBuilder: (context, imageProvider) => Container(
                                        width: MediaQuery.of(Get.context!).size.width,
                                        height: GlobalVariable.ratioWidth(Get.context!) * 200,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                        ),
                                      ),
                                      placeholder: (context, url) => Container(color: Colors.white, width: MediaQuery.of(Get.context!).size.width, height: GlobalVariable.ratioWidth(Get.context!) * 200, child: Center(child: CircularProgressIndicator())),
                                      errorWidget: (context, url, error) => Container(color: Colors.white, width: MediaQuery.of(Get.context!).size.width, height: GlobalVariable.ratioWidth(Get.context!) * 200, child: Center(child: Icon(Icons.hide_image_outlined))),
                                    )),
                                Container(
                                  width: MediaQuery.of(Get.context!).size.width,
                                  height: GlobalVariable.ratioWidth(Get.context!) * 200,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                          Colors.transparent,
                                          Colors.black
                                          //add more colors for gradient
                                        ],
                                        begin: Alignment.topCenter, //begin of the gradient color
                                        end: Alignment.bottomCenter, //end of the gradient color
                                        stops: [0.4, 1] //stops for individual color
                                        //set the stops number equal to numbers of color
                                        ),
                                  ),
                                ),
                                Positioned(
                                    right: GlobalVariable.ratioWidth(Get.context!) * 10,
                                    bottom: GlobalVariable.ratioWidth(Get.context!) * 10,
                                    child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: GestureDetector(
                                          onTap: () async {
                                            var data = await GetToPage.toNamed<ProfilController>(Routes.PROFIL,
                                                arguments: ["GEREJA", GlobalVariable.idGereja, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);

                                            if (data) {
                                              controller.callData();
                                            }
                                          },
                                          child: Row(children: [
                                            SizedBox(
                                                width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 60,
                                                child: Text("GBI " + controller.dataBPD['NAMAGEREJA'],
                                                    overflow: TextOverflow.ellipsis,
                                                    textAlign: TextAlign.right,
                                                    maxLines: 2,
                                                    style: TextStyle(
                                                      fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.white,
                                                    ))),
                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                            ClipRRect(
                                                borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                                child: Container(
                                                    height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                    width: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                    child: CachedNetworkImage(
                                                      imageUrl: controller.dataBPD['GAMBARGEREJA'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                      imageBuilder: (context, imageProvider) => Container(
                                                        height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                        decoration: BoxDecoration(
                                                          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                      placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 32, child: CircularProgressIndicator()),
                                                      errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 32, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                    )))
                                          ])),
                                    )),
                              ],
                            ))),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16, vertical: GlobalVariable.ratioWidth(Get.context!) * 14),
                          child: GestureDetector(
                              onTap: () async {
                                var data = await GetToPage.toNamed<ProfilController>(Routes.PROFIL,
                                    arguments: ["PEJABAT", GlobalVariable.idPejabat, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);

                                if (data) {
                                  controller.callData();
                                }
                              },
                              child: Row(children: [
                                Material(
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                    elevation: GlobalVariable.ratioWidth(Get.context!) * 6,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                      child: Container(
                                          height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                          width: GlobalVariable.ratioWidth(Get.context!) * 50,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                          ),
                                          child: CachedNetworkImage(
                                            imageUrl: controller.dataBPD['GAMBARPEJABAT'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                            imageBuilder: (context, imageProvider) => Container(
                                              height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                              ),
                                            ),
                                            placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: CircularProgressIndicator()),
                                            errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: Center(child: Icon(Icons.hide_image_outlined))),
                                          )),
                                    )),
                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 11),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Shallom,",
                                      style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                    ),
                                    Text(
                                      (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT'],
                                      style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.bold),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                )
                              ])),
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                            padding: EdgeInsets.symmetric(
                              vertical: GlobalVariable.ratioWidth(Get.context!) * 4,
                              horizontal: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            decoration: BoxDecoration(boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                offset: Offset(0.0, 8.0), //(x,y)
                                blurRadius: 20.0,
                              ),
                            ], color: Colors.white, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10), border: Border.all(color: AppColors.border, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                            child: Row(
                              children: [
                                GlobalVariable.statusGembala
                                    ? Expanded(
                                        child: GestureDetector(
                                            onTap: () {
                                              GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN,
                                                  arguments: ["PERPULUHAN", GlobalVariable.idGereja, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);
                                            },
                                            child: Row(
                                              children: [
                                                Obx(() => !controller.loadingPerpuluhanIuran.value
                                                    ? Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                        Text(
                                                          LocaleLanguage().tr(LocaleKeys.PERPULUHAN),
                                                          style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                                                        ),
                                                        SizedBox(
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 4,
                                                        ),
                                                        Row(
                                                          children: [
                                                            SvgPicture.asset("assets/icon/calendar.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Text(
                                                              GlobalVariable.cekBulan(controller.dataPerpuluhanIuran['perpuluhan']['bulan'].toString()).toString() + " " + controller.dataPerpuluhanIuran['perpuluhan']['tahun'],
                                                              style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 4,
                                                        ),
                                                        Row(
                                                          children: [
                                                            SvgPicture.asset("assets/icon/money.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Text(
                                                              "Rp" + GlobalVariable.formatCurrencyDecimal(controller.dataPerpuluhanIuran['perpuluhan']['nilai']).toString(),
                                                              style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                            ),
                                                          ],
                                                        )
                                                      ])
                                                    : Center(child: CircularProgressIndicator())),
                                                Expanded(child: Text("")),
                                                SvgPicture.asset("assets/icon/chevron_right.svg", width: GlobalVariable.ratioWidth(Get.context!) * 24, height: GlobalVariable.ratioWidth(Get.context!) * 24),
                                              ],
                                            )))
                                    : SizedBox(),
                                GlobalVariable.statusGembala
                                    ? Container(
                                        margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                                        height: GlobalVariable.ratioWidth(Get.context!) * 45,
                                        child: VerticalDivider(
                                          color: AppColors.divider,
                                          width: GlobalVariable.ratioWidth(Get.context!) * 1,
                                          thickness: GlobalVariable.ratioWidth(Get.context!) * 1,
                                        ))
                                    : SizedBox(),
                                Expanded(
                                    child: GestureDetector(
                                        onTap: () {
                                          GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN,
                                              arguments: ["IURAN", GlobalVariable.idPejabat, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);
                                        },
                                        child: Row(
                                          children: [
                                            Obx(() => !controller.loadingPerpuluhanIuran.value
                                                ? Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                    Text(
                                                      LocaleLanguage().tr(LocaleKeys.IURAN),
                                                      style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                                                    ),
                                                    SizedBox(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 4,
                                                    ),
                                                    Row(
                                                      children: [
                                                        SvgPicture.asset("assets/icon/calendar.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                        Text(
                                                          GlobalVariable.cekBulan(controller.dataPerpuluhanIuran['iuran']['bulan'].toString()).toString() + " " + controller.dataPerpuluhanIuran['iuran']['tahun'],
                                                          style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 4,
                                                    ),
                                                    Row(
                                                      children: [
                                                        SvgPicture.asset("assets/icon/money.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                        Text(
                                                          "Rp" + GlobalVariable.formatCurrencyDecimal(controller.dataPerpuluhanIuran['iuran']['nilai']).toString(),
                                                          style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                        ),
                                                      ],
                                                    )
                                                  ])
                                                : Center(child: CircularProgressIndicator())),
                                            Expanded(child: Text("")),
                                            SvgPicture.asset("assets/icon/chevron_right.svg", width: GlobalVariable.ratioWidth(Get.context!) * 24, height: GlobalVariable.ratioWidth(Get.context!) * 24),
                                          ],
                                        ))),
                              ],
                            )),
                        SizedBox(
                          height: GlobalVariable.ratioWidth(context) * 14,
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: GlobalVariable.statusGembala
                                  ? [
                                      controller.menu(() {
                                        GetToPage.toNamed<SuratController>(Routes.SURAT);
                                      }, "assets/icon/mail.svg", AppColors.menuOrange, LocaleLanguage().tr(LocaleKeys.SURAT)),
                                      controller.menu(() {
                                        GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN,
                                            arguments: ["PERPULUHAN", GlobalVariable.idGereja, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);
                                      }, "assets/icon/percentage.svg", AppColors.menuBiru, LocaleLanguage().tr(LocaleKeys.PERPULUHAN)),
                                      controller.menu(() {
                                        GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN,
                                            arguments: ["IURAN", GlobalVariable.idPejabat, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);
                                      }, "assets/icon/money_bag.svg", AppColors.menuHijau, LocaleLanguage().tr(LocaleKeys.IURAN)),
                                      controller.menu(() {
                                        GetToPage.toNamed<BPDController>(Routes.BPD);
                                      }, "assets/icon/phone.svg", AppColors.menuCokelat, 'BPD'),
                                      controller.menu(() {
                                        GetToPage.toNamed<MateriController>(Routes.MATERI);
                                      }, "assets/icon/book.svg", AppColors.menuAbuAbu, LocaleLanguage().tr(LocaleKeys.MATERIKHOTBAH), khusus: true),
                                    ]
                                  : [
                                      controller.menu(() {
                                        GetToPage.toNamed<SuratController>(Routes.SURAT);
                                      }, "assets/icon/mail.svg", AppColors.menuOrange, LocaleLanguage().tr(LocaleKeys.SURAT)),
                                      controller.menu(() {
                                        GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN,
                                            arguments: ["IURAN", GlobalVariable.idPejabat, "GBI " + controller.dataBPD['NAMAGEREJA'], (controller.dataBPD['JABATAN'] != "-" ? (controller.dataBPD['JABATAN'] + " ") : "") + controller.dataBPD['NAMAPEJABAT']]);
                                      }, "assets/icon/money_bag.svg", AppColors.menuHijau, LocaleLanguage().tr(LocaleKeys.IURAN)),
                                      controller.menu(() {
                                        GetToPage.toNamed<SuratController>(Routes.BPD);
                                      }, "assets/icon/phone.svg", AppColors.menuCokelat, 'BPD'),
                                      controller.menu(() {
                                        GetToPage.toNamed<MateriController>(Routes.MATERI);
                                      }, "assets/icon/book.svg", AppColors.menuAbuAbu, LocaleLanguage().tr(LocaleKeys.MATERIKHOTBAH), khusus: true),
                                    ],
                            )),
                        GlobalVariable.statusGembala
                            ? SizedBox(
                                height: GlobalVariable.ratioWidth(context) * 14,
                              )
                            : SizedBox(),
                        GlobalVariable.statusGembala
                            ? Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                                      ),
                                      child: Row(
                                        children: [
                                          Text(
                                            LocaleLanguage().tr(LocaleKeys.INFORMASIPEJABAT),
                                            style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 5),
                                          controller.dataInformasiPejabat.length > 0
                                              ? Container(
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(color: AppColors.border, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60)),
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 20,
                                                  height: GlobalVariable.ratioWidth(Get.context!) * 20,
                                                  child: Text(
                                                    controller.dataInformasiPejabat.length.toString(),
                                                    style: TextStyle(fontSize: AppFontSize.small),
                                                  ),
                                                )
                                              : SizedBox()
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                    controller.loadingInformasiPejabat.value
                                        ? Center(
                                            child: CircularProgressIndicator(),
                                          )
                                        : controller.dataInformasiPejabat.length > 0
                                            ? SingleChildScrollView(
                                                scrollDirection: Axis.horizontal,
                                                child: Row(children: [
                                                  for (var x = 0; x < controller.dataInformasiPejabat.length; x++)
                                                    GestureDetector(
                                                        onTap: () async {
                                                          var data = await GetToPage.toNamed<ProfilController>(Routes.PROFIL, arguments: ["PEJABAT", controller.dataInformasiPejabat[x]['idpejabat'], "GBI " + controller.dataBPD['NAMAGEREJA'], controller.dataInformasiPejabat[x]['namapejabat']]);

                                                          if (data) {
                                                            controller.callData();
                                                          }
                                                        },
                                                        child: Container(
                                                          decoration: BoxDecoration(border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1), borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6)),
                                                          margin: EdgeInsets.only(left: x == 0 ? GlobalVariable.ratioWidth(Get.context!) * 16 : GlobalVariable.ratioWidth(Get.context!) * 10, right: x == controller.dataInformasiPejabat.length - 1 ? GlobalVariable.ratioWidth(Get.context!) * 16 : 0),
                                                          width: GlobalVariable.ratioWidth(Get.context!) * 286,
                                                          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                            Container(
                                                                width: GlobalVariable.ratioWidth(Get.context!) * 122,
                                                                height: GlobalVariable.ratioWidth(Get.context!) * 150,
                                                                decoration: BoxDecoration(
                                                                    color: Colors.white,
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: AppColors.border,
                                                                        blurRadius: 4,
                                                                        offset: Offset(4, 0), // Shadow position
                                                                      ),
                                                                    ],
                                                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6)),
                                                                child: CachedNetworkImage(
                                                                  imageUrl: controller.dataInformasiPejabat[x]['imagepejabat'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                                  imageBuilder: (context, imageProvider) => ClipRRect(
                                                                      borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 6)),
                                                                      child: Container(
                                                                        height: GlobalVariable.ratioWidth(Get.context!) * 150,
                                                                        width: GlobalVariable.ratioWidth(Get.context!) * 122,
                                                                        decoration: BoxDecoration(
                                                                          image: DecorationImage(
                                                                            image: imageProvider,
                                                                            fit: BoxFit.cover,
                                                                          ),
                                                                        ),
                                                                      )),
                                                                  placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 150, child: CircularProgressIndicator()),
                                                                  errorWidget: (context, url, error) => Container(color: Colors.white, width: GlobalVariable.ratioWidth(Get.context!) * 122, height: GlobalVariable.ratioWidth(Get.context!) * 150, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                                )),
                                                            Container(
                                                                margin: EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 10, horizontal: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                child: Column(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Container(
                                                                        height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                                        child: Row(
                                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: [
                                                                            SvgPicture.asset("assets/icon/profile.svg", color: Colors.black, width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                            Container(
                                                                                width: GlobalVariable.ratioWidth(Get.context!) * 130,
                                                                                child: Text(
                                                                                  controller.dataInformasiPejabat[x]['namapejabat'],
                                                                                  style: TextStyle(fontSize: AppFontSize.small, fontWeight: FontWeight.w600),
                                                                                  maxLines: 2,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                ))
                                                                          ],
                                                                        )),
                                                                    SizedBox(
                                                                      height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                                                    ),
                                                                    Container(
                                                                        child: Row(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      children: [
                                                                        SvgPicture.asset("assets/icon/whatsapp.svg", color: Colors.black, width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                        Text(
                                                                          controller.dataInformasiPejabat[x]['telppejabat'],
                                                                          style: TextStyle(fontSize: AppFontSize.small),
                                                                          maxLines: 2,
                                                                          overflow: TextOverflow.ellipsis,
                                                                        )
                                                                      ],
                                                                    )),
                                                                    SizedBox(
                                                                      height: GlobalVariable.ratioWidth(Get.context!) * 6,
                                                                    ),
                                                                    Container(
                                                                        child: Row(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      children: [
                                                                        SvgPicture.asset("assets/icon/calendar.svg", color: Colors.black, width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                        Text(
                                                                          controller.dataInformasiPejabat[x]['tgliuran'],
                                                                          style: TextStyle(fontSize: AppFontSize.small),
                                                                          maxLines: 2,
                                                                          overflow: TextOverflow.ellipsis,
                                                                        )
                                                                      ],
                                                                    )),
                                                                    SizedBox(
                                                                      height: GlobalVariable.ratioWidth(Get.context!) * 6,
                                                                    ),
                                                                    Container(
                                                                        child: Row(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      children: [
                                                                        SvgPicture.asset("assets/icon/money.svg", color: Colors.black, width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                        Text(
                                                                          "Rp" + GlobalVariable.formatCurrencyDecimal(controller.dataInformasiPejabat[x]['amountiuran']).toString(),
                                                                          style: TextStyle(fontSize: AppFontSize.small),
                                                                          maxLines: 2,
                                                                          overflow: TextOverflow.ellipsis,
                                                                        )
                                                                      ],
                                                                    )),
                                                                  ],
                                                                )),
                                                          ]),
                                                        ))
                                                ]),
                                              )
                                            : Container(
                                                alignment: Alignment.center,
                                                margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                                                height: GlobalVariable.ratioWidth(Get.context!) * 28,
                                                child: Text(LocaleLanguage().tr(LocaleKeys.TIDAKADADATAPEJABAT), style: TextStyle(fontSize: AppFontSize.small))),
                                    SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                  ],
                                ),
                              )
                            : SizedBox(),
                        //INCOMING EVENT
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      LocaleLanguage().tr(LocaleKeys.KEGIATANAKANDATANG),
                                      style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 5),
                                    controller.dataIncomingEvent.length > 0
                                        ? Container(
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(color: AppColors.border, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60)),
                                            width: GlobalVariable.ratioWidth(Get.context!) * 20,
                                            height: GlobalVariable.ratioWidth(Get.context!) * 20,
                                            child: Text(
                                              controller.dataIncomingEvent.length.toString(),
                                              style: TextStyle(fontSize: AppFontSize.small),
                                            ),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 20),
                              controller.loadingIncomingEvent.value
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : controller.dataIncomingEvent.length > 0
                                      ? SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                            for (var x = 0; x < controller.dataIncomingEvent.length; x++)
                                              GestureDetector(
                                                  onTap: () {
                                                    GetToPage.toNamed<KegiatanController>(Routes.KEGIATAN, arguments: ["INCOMING", controller.dataIncomingEvent[x]['idevent']]);
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: x == 0 ? GlobalVariable.ratioWidth(Get.context!) * 16 : GlobalVariable.ratioWidth(Get.context!) * 10, right: x == controller.dataIncomingEvent.length - 1 ? GlobalVariable.ratioWidth(Get.context!) * 16 : 0),
                                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                    child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                      Container(
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 185,
                                                          width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                          decoration: BoxDecoration(color: Colors.black, border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1), borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6)),
                                                          child: CachedNetworkImage(
                                                            imageUrl: controller.dataIncomingEvent[x]['imageevent'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                            imageBuilder: (context, imageProvider) => ClipRRect(
                                                                borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                child: Container(
                                                                  height: GlobalVariable.ratioWidth(Get.context!) * 185,
                                                                  width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                                  decoration: BoxDecoration(
                                                                    image: DecorationImage(
                                                                      image: imageProvider,
                                                                      fit: BoxFit.contain,
                                                                    ),
                                                                  ),
                                                                )),
                                                            placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 185, child: CircularProgressIndicator()),
                                                            errorWidget: (context, url, error) => Container(color: Colors.white, width: GlobalVariable.ratioWidth(Get.context!) * 148, height: GlobalVariable.ratioWidth(Get.context!) * 185, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                          )),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                        child: Text(
                                                          controller.dataIncomingEvent[x]['namaevent'],
                                                          style: TextStyle(
                                                            fontSize: AppFontSize.medium,
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                          maxLines: 2,
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                            SvgPicture.asset("assets/icon/calendar.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Text(
                                                              controller.dataIncomingEvent[x]['tglevent'],
                                                              style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            SvgPicture.asset("assets/icon/home.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Expanded(
                                                              child: Text(
                                                                controller.dataIncomingEvent[x]['lokasievent'],
                                                                style: TextStyle(
                                                                  fontSize: AppFontSize.small,
                                                                ),
                                                                maxLines: 2,
                                                                overflow: TextOverflow.ellipsis,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ]),
                                                  ))
                                          ]),
                                        )
                                      : Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                                          height: GlobalVariable.ratioWidth(Get.context!) * 28,
                                          child: Text(LocaleLanguage().tr(LocaleKeys.TIDAKADADATAINCOMINGEVENT), style: TextStyle(fontSize: AppFontSize.small))),
                              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 20),
                            ],
                          ),
                        ),
                        //OUTCOMING EVENT
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      LocaleLanguage().tr(LocaleKeys.KEGIATANTELAHSELESAI),
                                      style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 5),
                                    controller.dataOutcomingEvent.length > 0
                                        ? Container(
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(color: AppColors.border, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60)),
                                            width: GlobalVariable.ratioWidth(Get.context!) * 20,
                                            height: GlobalVariable.ratioWidth(Get.context!) * 20,
                                            child: Text(
                                              controller.dataOutcomingEvent.length.toString(),
                                              style: TextStyle(fontSize: AppFontSize.small),
                                            ),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 20),
                              controller.loadingOutcomingEvent.value
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : controller.dataOutcomingEvent.length > 0
                                      ? SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                            for (var x = 0; x < controller.dataOutcomingEvent.length; x++)
                                              GestureDetector(
                                                  onTap: () {
                                                    GetToPage.toNamed<KegiatanController>(Routes.KEGIATAN, arguments: ["OUTCOMING", controller.dataOutcomingEvent[x]['idevent']]);
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: x == 0 ? GlobalVariable.ratioWidth(Get.context!) * 16 : GlobalVariable.ratioWidth(Get.context!) * 10, right: x == controller.dataOutcomingEvent.length - 1 ? GlobalVariable.ratioWidth(Get.context!) * 16 : 0),
                                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                    child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                      Container(
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 185,
                                                          width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                          decoration: BoxDecoration(color: Colors.black, border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1), borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6)),
                                                          child: CachedNetworkImage(
                                                            imageUrl: controller.dataOutcomingEvent[x]['imageevent'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                            imageBuilder: (context, imageProvider) => ClipRRect(
                                                                borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 6),
                                                                child: Container(
                                                                  height: GlobalVariable.ratioWidth(Get.context!) * 185,
                                                                  width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                                                  decoration: BoxDecoration(
                                                                    image: DecorationImage(
                                                                      image: imageProvider,
                                                                      fit: BoxFit.contain,
                                                                    ),
                                                                  ),
                                                                )),
                                                            placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 185, child: CircularProgressIndicator()),
                                                            errorWidget: (context, url, error) => Container(color: Colors.white, width: GlobalVariable.ratioWidth(Get.context!) * 148, height: GlobalVariable.ratioWidth(Get.context!) * 185, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                          )),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        height: GlobalVariable.ratioWidth(Get.context!) * 32,
                                                        child: Text(
                                                          controller.dataOutcomingEvent[x]['namaevent'],
                                                          style: TextStyle(
                                                            fontSize: AppFontSize.medium,
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                          maxLines: 2,
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        child: Row(
                                                          children: [
                                                            SvgPicture.asset("assets/icon/calendar.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Text(
                                                              controller.dataOutcomingEvent[x]['tglevent'],
                                                              style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 12),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                      Container(
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            SvgPicture.asset("assets/icon/home.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                            SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                                            Expanded(
                                                              child: Text(
                                                                controller.dataOutcomingEvent[x]['lokasievent'],
                                                                style: TextStyle(
                                                                  fontSize: AppFontSize.small,
                                                                ),
                                                                maxLines: 2,
                                                                overflow: TextOverflow.ellipsis,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ]),
                                                  ))
                                          ]),
                                        )
                                      : Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                                          height: GlobalVariable.ratioWidth(Get.context!) * 28,
                                          child: Text(LocaleLanguage().tr(LocaleKeys.TIDAKADADATAOUTCOMINGEVENT), style: TextStyle(fontSize: AppFontSize.small))),
                              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 20),
                            ],
                          ),
                        )
                      ],
                    )),
                  ))));
  }
}
