import 'package:alpha_bpd_apk/app/modules/materi/controllers/materi_controller.dart';
import 'package:get/get.dart';

class MateriBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MateriController());
  }
}
