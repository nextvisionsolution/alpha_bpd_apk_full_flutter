import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MateriController extends GetxController {
  var dataMateri = [].obs;
  var loading = false.obs;
  var search = false.obs;
  var searchController = TextEditingController(text: '').obs;
  //TODO: Implement HomeController

  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    await getDataMateri();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  onChange(value) {
    if (value != "") {
      search.value = true;
    } else {
      search.value = false;
    }
  }

  getDataMateri() async {
    loading.value = true;
    dataMateri.clear();
    var data = await ApiHelper().getDataMateri(searchController.value.text);
    print(data);
    if (data['status'] == 'OK') {
      for (var x = 0; x < data['data'].length; x++) {
        var detail = {
          'judul': data['data'][x]['NAMAKHOTBAH'],
          'link': data['data'][x]['FILE'],
        };
        dataMateri.add(detail);
      }
    }
    dataMateri.refresh();
    loading.value = false;
  }
}
