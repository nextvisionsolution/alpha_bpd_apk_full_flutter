import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/home/controllers/home_controller.dart';
import 'package:alpha_bpd_apk/app/modules/login/controllers/login_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class SplashController extends GetxController {
  //TODO: Implement HomeController

  final count = 0.obs;
  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
    WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
    FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
    initialization();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void initialization() async {
    // This is where you can initialize the resources needed by your app while
    // the splash screen is displayed.  Remove the following example because
    // delaying the user experience is a bad design practice!
    // ignore_for_file: avoid_print

    String codeLanguage = await SharedPreferencesHelper.getCodeLanguage();
    Map<String, dynamic> dataLanguage = {};
    try {
      ApiHelper().init();
      var response = await ApiHelper().getLanguage();
      if (response != null && response['status'] == "OK") {
        dataLanguage = response['data'] as Map<String, dynamic>;
      } else {
        print("=======================");
        print("||Failed Get Language||");
        print("=======================");
      }
    } catch (e) {
      print(e);
      print("=======================");
      print("||Failed Get Language||");
      print("=======================");
    }

    FlutterNativeSplash.remove();
    await lang.init(dataLanguage, codeLanguage);
    print('ready in 3...');
    await Future.delayed(const Duration(seconds: 1));
    print('ready in 2...');
    await Future.delayed(const Duration(seconds: 1));
    print('ready in 1...');
    await Future.delayed(const Duration(seconds: 1));
    print('go!');

    Map data = await SharedPreferencesHelper.getDataLogin();

    if (data.isEmpty) {
      GetToPage.toNamed<LoginController>(
        Routes.LOGIN,
      );
    } else {
      GetToPage.toNamed<HomeController>(
        Routes.HOME,
      );
    }
  }
}
