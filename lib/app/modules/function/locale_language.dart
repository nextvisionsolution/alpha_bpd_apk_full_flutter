import 'dart:convert';

import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class LocaleLanguage extends GetxController {
  static RxString codeLanguange = "".obs;
  var jsonName = {"id": "id.json", "en": "en.json"};
  static Map<String, dynamic>? jsonText;
  Future init(Map<String, dynamic> responseData, String code) async {
    codeLanguange.value = code;
    jsonText = responseData;
  }

  String tr(String key) {
    String word = key;
    word = jsonText == null ? "" : jsonText![codeLanguange][key] ?? key;
    return word;
  }

  setCodeLanguage(String code) {
    codeLanguange.value = code;
    SharedPreferencesHelper.setCodeLanguage(code);
    Get.offNamedUntil(Routes.SPLASH, (route) => false);
  }
}
