import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import "package:dio/src/form_data.dart" as fd;

enum TypeReq { POST, GET, DOWNLOAD }

class ApiHelper {
  final String urlPath = GlobalVariable.apiPath;

  final BuildContext? context;
  final bool isShowDialogLoading;
  final bool isShowDialogError;
  final bool isDebugGetResponse;

  bool _isShowingDialogLoading = false;
  bool isError = false;
  bool isErrorConnection = false;
  bool isErrorResponse = false;
  var errorMessage = "";
  static final dio = Dio();

  ApiHelper({this.context, this.isShowDialogLoading = true, this.isShowDialogError = true, this.isDebugGetResponse = false});

  Future _checkConnection() async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi) {
        isError = true;
      }
    } catch (err) {
      isError = true;
    }
    if (isError) {
      _setError(isErrorConnection, true, "Error");
    }
  }

  void _setError(bool isErrorConnection, bool isErrorResponse, var errorMessage) {
    isError = true;
    this.isErrorConnection = isErrorConnection;
    this.isErrorResponse = isErrorResponse;
    this.errorMessage = errorMessage;
  }

  init() {
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  Future sendRequestAPI({required String urlAPI, dynamic body, TypeReq tipe = TypeReq.POST}) async {
    await _checkConnection();
    var response;
    try {
      print("Url API = $urlAPI");
      print("Url BODY = $body");
      if (tipe == TypeReq.POST) {
        final formData = fd.FormData.fromMap(body ?? {});
        response = await dio.post(urlAPI, data: formData);
      } else if (tipe == TypeReq.GET) {
        response = await dio.get(urlAPI, queryParameters: body ?? {});
      }
      if (response.statusCode == 200) {
        return json.decode(response.data);
      } else {
        print("=======================");
        print("||Start Error Message||");
        print("=======================");
        print("Error code : " + response.statusCode.toString());
        print("Error message: " + response.statusMessage);
        print("=======================");
        print("||End Error Message  ||");
        print("=======================");
        return null;
      }
    } catch (e) {
      print("=======================");
      print("||Start Error Message||");
      print("=======================");
      print(e);
      print("=======================");
      print("||End Error Message  ||");
      print("=======================");
      return null;
    }
  }

  Future getLanguage() async {
    String path = "Api/Api/getBahasa";
    return sendRequestAPI(urlAPI: urlPath + path);
  }

  login(user, password) async {
    String path = "Api/Api/Login";
    var body = {'userid': user, 'password': password};
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataPejabat() async {
    String path = "Api/Api/pejabatView/";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idpejabat': GlobalVariable.idPejabat};
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataPerpuluhanIuranTerakhir() async {
    String path = "Api/Api/perpuluhanIuranTerakhir/";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idgereja': GlobalVariable.idGereja, 'idpejabat': GlobalVariable.idPejabat};
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataInformasiPejabat() async {
    String path = "Api/Api/getListPejabat";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'kodegereja': GlobalVariable.kodeGereja, 'idpejabat': GlobalVariable.idPejabat};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataIncomingEvent() async {
    String path = "Api/Api/getListIncomingEvent";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataOutcomingEvent() async {
    String path = "Api/Api/getListOutcomingEvent";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getTahunPerpuluhan(idgereja) async {
    String path = "Api/Api/getFilterTahunPerpuluhan";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idgereja': idgereja};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getTahunIuran(idpejabat) async {
    String path = "Api/Api/getFilterTahunIuran";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idpejabat': idpejabat};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataPerpuluhan(idgereja, tahun) async {
    String path = "Api/Api/getDetailPerpuluhan";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idgereja': idgereja, 'tahun': tahun};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataIuran(idpejabat, tahun) async {
    String path = "Api/Api/getDetailIuran";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan, 'idpejabat': idpejabat, 'idgereja': GlobalVariable.idGereja, 'tahun': tahun};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataMateri(keyword) async {
    String path = "Api/Api/getListKotbah";
    var body = {'keyword': keyword, 'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataSurat(keyword) async {
    String path = "Api/Api/getListSurat";
    var body = {'keyword': keyword, 'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getDataKegiatan(idkegiatan) async {
    String path = "Api/Api/getDetailKegiatan";
    var body = {'idkegiatan': idkegiatan, 'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  daftarKegiatan(idkegiatan, nama, telp, catatan, jenisDaftar) async {
    String path = "Api/Api/daftarKegiatan";
    var body;
    print(jenisDaftar);
    if (jenisDaftar == "ORANGLAIN") {
      body = {'idkegiatan': idkegiatan, 'nama': nama, 'telp': telp, 'catatan': catatan, 'idpejabat': '0', 'idperusahaan': GlobalVariable.idPerusahaan};
    } else {
      body = {'idkegiatan': idkegiatan, 'nama': nama, 'telp': telp, 'catatan': catatan, 'idpejabat': GlobalVariable.idPejabat, 'idperusahaan': GlobalVariable.idPerusahaan};
    }

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getProfilBPD() async {
    String path = "Api/Api/getProfilBPD";
    var body = {'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getProfilGereja(idgereja) async {
    String path = "Api/Api/getProfilGereja";
    var body = {'idgereja': idgereja, 'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  getProfilPejabat(String idpejabat) async {
    String path = "Api/Api/getProfilPejabat";
    var body = {'idpejabat': idpejabat, 'idperusahaan': GlobalVariable.idPerusahaan};

    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  simpanProfilGereja(String idgereja, String namagereja, String email, String telp, String fax, String alamat, String gambar, String gambarbanner) async {
    String path = "Api/Api/editProfilgereja";
    var body = {'idgereja': idgereja, 'namagereja': namagereja, 'email': email, 'telp': telp, 'fax': fax, 'alamat': alamat, 'gambar': gambar, 'gambarbanner': gambarbanner, 'idperusahaan': GlobalVariable.idPerusahaan};

    print(body);
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  simpanProfilPejabat(String idpejabat, String namapejabat, String email, String telp, String fax, String alamat, String gambar) async {
    String path = "Api/Api/editProfilPejabat";
    var body = {'idpejabat': idpejabat, 'namapejabat': namapejabat, 'email': email, 'telp': telp, 'fax': fax, 'alamat': alamat, 'gambar': gambar, 'idperusahaan': GlobalVariable.idPerusahaan};
    print(body);
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }

  gantiPassword(String idpejabat, String passwordlama, String passwordbaru) async {
    String path = "Api/Api/ubahPasswordPejabat";
    var body = {'idpejabat': idpejabat, 'passwordlama': passwordlama, 'passwordbaru': passwordbaru};
    print(body);
    return sendRequestAPI(urlAPI: urlPath + path, body: body);
  }
}
