import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/login/controllers/login_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_cli/common/utils/json_serialize/json_ast/utils/substring.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:url_launcher/url_launcher.dart';

class GlobalVariable {
  static String apiPath = "https://192.168.1.73/alpha_bpd/";

  //static String apiPath = "https://bpd.alphasolution.id/";
  static String imagePath = "https";
  static String lang = "id"; //id, en
  static String assetLanguage = "assets/translations/";
  static String version = "1.0.0";

  bool _isShowingDialogLoading = false;
  bool isError = false;
  bool isErrorConnection = false;
  bool isErrorResponse = false;

  static String idPerusahaan = "";
  static String idPejabat = "";
  static String idGereja = "";
  static String kodeGereja = "";
  static bool statusGembala = false;
  Map<String, dynamic>? translate;

  static double ratioWidth(BuildContext context) {
    return MediaQuery.of(context).size.width / 360;
  }

  static double ratioFontSize(BuildContext context) {
    return ratioWidth(context);
  }

  static void errorPopUp(message) async {
    CoolAlert.show(context: Get.context!, type: CoolAlertType.error, text: message);
  }

  static void successPopUp(message) async {
    CoolAlert.show(context: Get.context!, type: CoolAlertType.success, text: message);
  }

  static void loadingPopUp(message) async {
    CoolAlert.show(context: Get.context!, type: CoolAlertType.loading, text: message);
  }

  static String formatDate(String date) {
    String newDate = "";
    newDate = date.split("-")[2] + " " + GlobalVariable.cekBulan(int.parse(date.split("-")[1]).toString()) + " " + date.split("-")[0];
    return newDate;
  }

  static String formatTime(String time) {
    String newTime = "";
    newTime = time.split(":")[0] + "." + time.split(":")[1];
    return newTime;
  }

  static String formatCurrencyDecimal(String text) {
    String newText = '';
    var firstTime = true;
    var subsText = '';

    var arrText = text.toString().split(".");

    for (var i = arrText[0].length - 1; i >= 0; i--) {
      if ((subsText.length == 3 && firstTime) || (subsText.length == 2 && !firstTime)) {
        subsText = "";
        newText += "." + arrText[0][i];
        firstTime = false;
      } else {
        subsText += arrText[0][i];
        newText += arrText[0][i];
      }
    }

    //JIKA MENGANDUNG KOMA
    if (arrText.length > 1) {
      if (int.parse(arrText[1]) != 0) {
        arrText[1] = arrText[1].split('').reversed.join('');
        newText = arrText[1] + "," + newText;
      }
    }

    newText = newText.split('').reversed.join('');
    return newText;
  }

  static String convertToBase64(File file) {
    if (file.path != "") {
      String? nama = file.path.toString().split("/").last;
      String? tipe = lookupMimeType(nama);
      String? data = "data:" + tipe! + ";base64," + base64Encode(file.readAsBytesSync());
      return data;
    } else {
      return '';
    }
  }

  static void logout() {
    GlobalVariable.confirmPopUp(LocaleLanguage().tr(LocaleKeys.LOGOUTQUESTION), () {
      Get.back();
    }, () {
      SharedPreferencesHelper.setDataLogin({});
      GetToPage.offAllNamed<LoginController>(Routes.LOGIN);
    });
  }

  static void out() {
    GlobalVariable.confirmPopUp(LocaleLanguage().tr(LocaleKeys.EXIT), () {
      Get.back();
    }, () {
      exit(0);
    });
  }

  static void confirmPopUp(String text, onTapNo, onTapYes) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(text, textAlign: TextAlign.center, style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 16,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: onTapNo,
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.NO),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: onTapYes,
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.YES),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1))))))
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }

  static void callingPopUp(String telp) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(child: Text("")),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          Get.back();
                                          if (telp[0] == "0") {
                                            telp = "62" + substring(telp, 1, telp.length);
                                          }

                                          if (!await launchUrl(Uri.parse('tel://' + telp))) {
                                            throw Exception('Could not launch');
                                          }
                                        },
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/phone.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              LocaleLanguage().tr(LocaleKeys.TELPON),
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          Get.back();
                                          if (telp[0] == "0") {
                                            telp = "62" + substring(telp, 1, telp.length);
                                          }

                                          if (!await launchUrl(Uri.parse('https://wa.me/' + telp))) {
                                            throw Exception('Could not launch');
                                          }
                                        },
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/whatsapp.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              "Whatsapp",
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Expanded(child: Text("")),
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }

  static File? picturePopUp(onPressCamera, onPressGallery) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(child: Text("")),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: onPressCamera,
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/camera-o.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              LocaleLanguage().tr(LocaleKeys.KAMERA),
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 90,
                                    child: ElevatedButton(
                                        onPressed: onPressGallery,
                                        child: Column(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icon/gallery.svg",
                                              width: GlobalVariable.ratioWidth(Get.context!) * 40,
                                              height: GlobalVariable.ratioWidth(Get.context!) * 40,
                                            ),
                                            Expanded(
                                              child: Text(""),
                                            ),
                                            Text(
                                              LocaleLanguage().tr(LocaleKeys.GALERI),
                                              style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500, color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), side: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Expanded(child: Text("")),
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }

  static String cekBulan(String bulan) {
    if (bulan == '1') {
      return LocaleLanguage().tr(LocaleKeys.JANUARI);
    } else if (bulan == '2') {
      return LocaleLanguage().tr(LocaleKeys.FEBRUARI);
    } else if (bulan == '3') {
      return LocaleLanguage().tr(LocaleKeys.MARET);
    } else if (bulan == '4') {
      return LocaleLanguage().tr(LocaleKeys.APRIL);
    } else if (bulan == '5') {
      return LocaleLanguage().tr(LocaleKeys.MEI);
    } else if (bulan == '6') {
      return LocaleLanguage().tr(LocaleKeys.JUNI);
    } else if (bulan == '7') {
      return LocaleLanguage().tr(LocaleKeys.JULI);
    } else if (bulan == '8') {
      return LocaleLanguage().tr(LocaleKeys.AGUSTUS);
    } else if (bulan == '9') {
      return LocaleLanguage().tr(LocaleKeys.SEPTEMBER);
    } else if (bulan == '10') {
      return LocaleLanguage().tr(LocaleKeys.OKTOBER);
    } else if (bulan == '11') {
      return LocaleLanguage().tr(LocaleKeys.NOVEMBER);
    } else if (bulan == '12') {
      return LocaleLanguage().tr(LocaleKeys.DESEMBER);
    }
    return bulan;
  }
}
