import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static SharedPreferences? _sharedPreferences;
  static String _lang = "language";
  static String _login = "login";

  static Future _init() async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
  }

  // get & set language
  static Future setCodeLanguage(String codeLanguage) async {
    await _init();
    _sharedPreferences!.setString(_lang, codeLanguage);
  }

  static Future<String> getCodeLanguage() async {
    await _init();
    String code = _sharedPreferences!.getString(
          _lang,
        ) ??
        "id";
    return code;
  }

  //get & set data login
  static Future setDataLogin(Map data) async {
    var stringData = jsonEncode(data);
    print("IN : " + stringData);
    await _init();
    _sharedPreferences!.setString(_login, stringData);
  }

  static Future<Map> getDataLogin() async {
    await _init();
    String stringData = _sharedPreferences!.getString(
          _login,
        ) ??
        "{}";
    print("OUT : " + stringData);
    return jsonDecode(stringData);
  }
}
