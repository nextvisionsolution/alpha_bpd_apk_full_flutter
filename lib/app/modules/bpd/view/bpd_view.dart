import 'package:alpha_bpd_apk/app/modules/bpd/controllers/bpd_controller.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/controllers/perpuluhan_iuran_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_cli/common/utils/json_serialize/json_ast/utils/substring.dart';
import 'package:latlong2/latlong.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

class BPDView extends GetView<BPDController> {
  const BPDView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Obx(() => controller.loading.value
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Scaffold(
                backgroundColor: Colors.white,
                // appBar: PreferredSize(
                //   preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
                //   child: Container(
                //       height: GlobalVariable.ratioWidth(Get.context!) * 56,
                //       decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1))),
                //       padding: EdgeInsets.symmetric(
                //         vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
                //         horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                //       ),
                //       child: Row(
                //         children: [
                //           GestureDetector(
                //             child: SvgPicture.asset("assets/icon/chevron_left.svg", width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 30),
                //             onTap: () {
                //               Get.back();
                //             },
                //           ),
                //           SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                //           Expanded(
                //             child: Text(
                //               "BPD",
                //               overflow: TextOverflow.ellipsis,
                //               style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large),
                //             ),
                //           )
                //         ],
                //       )),
                // ),
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: Stack(
                      children: [
                        Container(
                            width: MediaQuery.of(Get.context!).size.width,
                            height: GlobalVariable.ratioWidth(Get.context!) * 200,
                            child: CachedNetworkImage(
                              imageUrl: controller.dataBPD['GAMBARBANNER'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                              imageBuilder: (context, imageProvider) => Container(
                                width: MediaQuery.of(Get.context!).size.width,
                                height: GlobalVariable.ratioWidth(Get.context!) * 200,
                                decoration: BoxDecoration(
                                  image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) => Container(color: Colors.white, width: MediaQuery.of(Get.context!).size.width, height: GlobalVariable.ratioWidth(Get.context!) * 200, child: Center(child: CircularProgressIndicator())),
                              errorWidget: (context, url, error) => Container(color: Colors.white, width: MediaQuery.of(Get.context!).size.width, height: GlobalVariable.ratioWidth(Get.context!) * 200, child: Center(child: Icon(Icons.hide_image_outlined))),
                            )),
                        Container(
                          width: MediaQuery.of(Get.context!).size.width,
                          height: GlobalVariable.ratioWidth(Get.context!) * 200,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Colors.transparent,
                                  Colors.black,
                                  //add more colors for gradient
                                ],
                                begin: Alignment.bottomCenter, //begin of the gradient color
                                end: Alignment.topCenter, //end of the gradient color
                                stops: [0.001, 1] //stops for individual color
                                //set the stops number equal to numbers of color
                                ),
                          ),
                        ),
                        Positioned(
                            left: GlobalVariable.ratioWidth(Get.context!) * 16,
                            top: GlobalVariable.ratioWidth(Get.context!) * 25,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                child: SvgPicture.asset(
                                  "assets/icon/chevron_left.svg",
                                  width: GlobalVariable.ratioWidth(Get.context!) * 30,
                                  height: GlobalVariable.ratioWidth(Get.context!) * 30,
                                  color: Colors.white,
                                ),
                                onTap: () {
                                  Get.back();
                                },
                              ),
                            )),
                        Positioned(
                            right: GlobalVariable.ratioWidth(Get.context!) * 16,
                            top: GlobalVariable.ratioWidth(Get.context!) * 16,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Row(children: [
                                Container(
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                                    Text(
                                      "BPD " + controller.dataBPD['NAMAPERUSAHAAN'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large, color: Colors.white),
                                    ),
                                    Text(
                                      "Gereja Bethel Indonesia",
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large, color: Colors.white),
                                    ),
                                  ]),
                                ),
                                SizedBox(
                                  width: GlobalVariable.ratioWidth(Get.context!) * 10,
                                ),
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                    child: Container(
                                      height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                      width: GlobalVariable.ratioWidth(Get.context!) * 50,
                                      child: GestureDetector(
                                          onTap: () {},
                                          child: GestureDetector(
                                              onTap: () {},
                                              child: CachedNetworkImage(
                                                imageUrl: controller.dataBPD['GAMBAR'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                imageBuilder: (context, imageProvider) => Container(
                                                  height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                                  ),
                                                ),
                                                placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: CircularProgressIndicator()),
                                                errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: Center(child: Icon(Icons.hide_image_outlined))),
                                              ))),
                                    ))
                              ]),
                            )),
                      ],
                    )),
                    Expanded(
                        child: SingleChildScrollView(
                            child: Column(children: [
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 24),
                      GestureDetector(
                          onTap: () async {
                            if (controller.dataBPD['TELP'] != "") {
                              var telp = controller.dataBPD['TELP'];

                              if (telp[0] == "0") {
                                telp = "62" + substring(telp, 1, telp.length);
                              }

                              if (!await launchUrl(Uri.parse('tel://' + telp))) {
                                throw Exception('Could not launch');
                              }
                            }
                          },
                          child: Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: Row(
                                children: [
                                  SvgPicture.asset("assets/icon/phone.svg", width: GlobalVariable.ratioWidth(Get.context!) * 16, height: GlobalVariable.ratioWidth(Get.context!) * 16),
                                  SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                  Text(
                                    controller.dataBPD['TELP'] == "" ? "-" : controller.dataBPD['TELP'],
                                    style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ))),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                          child: Row(
                            children: [
                              SvgPicture.asset("assets/icon/fax.svg", width: GlobalVariable.ratioWidth(Get.context!) * 16, height: GlobalVariable.ratioWidth(Get.context!) * 16),
                              SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                              Text(
                                controller.dataBPD['FAX'] == "" ? "-" : controller.dataBPD['FAX'],
                                style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                              ),
                            ],
                          )),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      GestureDetector(
                          onTap: () async {
                            if (controller.dataBPD['WA'] != "") {
                              var telp = controller.dataBPD['WA'];

                              if (telp[0] == "0") {
                                telp = "62" + substring(telp, 1, telp.length);
                              }

                              if (!await launchUrl(Uri.parse('https://wa.me/' + telp))) {
                                throw Exception('Could not launch');
                              }
                            }
                          },
                          child: Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: Row(
                                children: [
                                  SvgPicture.asset("assets/icon/whatsapp.svg", width: GlobalVariable.ratioWidth(Get.context!) * 16, height: GlobalVariable.ratioWidth(Get.context!) * 16),
                                  SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                                  Text(
                                    controller.dataBPD['WA'] == "" ? "-" : controller.dataBPD['WA'],
                                    style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ))),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                          child: Row(
                            children: [
                              SvgPicture.asset("assets/icon/mail.svg", width: GlobalVariable.ratioWidth(Get.context!) * 16, height: GlobalVariable.ratioWidth(Get.context!) * 16),
                              SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                              Text(
                                controller.dataBPD['EMAIL'],
                                style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600),
                              ),
                            ],
                          )),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 2), child: SvgPicture.asset("assets/icon/address.svg", width: GlobalVariable.ratioWidth(Get.context!) * 16, height: GlobalVariable.ratioWidth(Get.context!) * 16)),
                              SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 4),
                              Expanded(
                                  child: Text(
                                controller.dataBPD['ALAMAT'],
                                style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w400),
                                maxLines: 5,
                              )),
                            ],
                          )),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      GestureDetector(
                          onTap: () async {
                            if (!await launchUrl(Uri.parse('https://www.google.com/maps/@' + (controller.dataBPD['LATITUDE'] ?? "0") + ',' + (controller.dataBPD['LONGITUDE'] ?? "0") + ',15z?entry=ttu'))) {
                              throw Exception('Could not launch');
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1), borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10)),
                            margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                            height: GlobalVariable.ratioWidth(Get.context!) * 200,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10),
                                child: IgnorePointer(
                                    child: FlutterMap(
                                  options: MapOptions(
                                    center: LatLng(double.parse(controller.dataBPD['LATITUDE'] == "" || controller.dataBPD['LATITUDE'] == null ? "0" : controller.dataBPD['LATITUDE']),
                                        double.parse(controller.dataBPD['LONGITUDE'] == "" || controller.dataBPD['LONGITUDE'] == null ? "0" : controller.dataBPD['LONGITUDE'])),
                                    zoom: 16.0,
                                  ),
                                  children: [
                                    TileLayer(
                                      urlTemplate: "https://api.mapbox.com/styles/v1/alpharayakreasi/ckxr82bak1yqg15lzibihsig1/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYWxwaGFyYXlha3JlYXNpIiwiYSI6ImNreHI3emFmdTVicGgzMHBmenE1bXNrOWEifQ.o7v-JijcyRzubVweHxlMUw",
                                      additionalOptions: {"accessToken": "pk.eyJ1IjoiYWxwaGFyYXlha3JlYXNpIiwiYSI6ImNreHI3emFmdTVicGgzMHBmenE1bXNrOWEifQ.o7v-JijcyRzubVweHxlMUw", "id": "Mapbox Streets v8"},
                                    ),
                                    MarkerLayer(markers: [
                                      Marker(
                                        point: LatLng(double.parse(controller.dataBPD['LATITUDE'] == "" || controller.dataBPD['LATITUDE'] == null ? "0" : controller.dataBPD['LATITUDE']),
                                            double.parse(controller.dataBPD['LONGITUDE'] == "" || controller.dataBPD['LONGITUDE'] == null ? "0" : controller.dataBPD['LONGITUDE'])),
                                        width: GlobalVariable.ratioWidth(Get.context!) * 33,
                                        height: GlobalVariable.ratioWidth(Get.context!) * 33,
                                        builder: (ctx) => Container(
                                          child: SvgPicture.asset(
                                            "assets/icon/pin.svg",
                                            width: GlobalVariable.ratioWidth(Get.context!) * 33,
                                            height: GlobalVariable.ratioWidth(Get.context!) * 33,
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ],
                                ))),
                          )),
                      SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                      for (var x = 0; x < controller.dataPengurusBPD.length; x++)
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            Text(controller.dataPengurusBPD[x]['jabatanpejabat'], style: TextStyle(fontSize: AppFontSize.small, fontWeight: FontWeight.w700)),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            GestureDetector(
                                onTap: () {
                                  if (controller.dataPengurusBPD[x]['telppejabat'] != "") {
                                    GlobalVariable.callingPopUp(controller.dataPengurusBPD[x]['telppejabat']);
                                  }
                                },
                                child: Container(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Material(
                                            borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                            elevation: GlobalVariable.ratioWidth(Get.context!) * 6,
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                              child: Container(
                                                  height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 50,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 60),
                                                  ),
                                                  child: CachedNetworkImage(
                                                    imageUrl: controller.dataPengurusBPD[x]['gambarpejabat'] + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                    imageBuilder: (context, imageProvider) => Container(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 50,
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                                      ),
                                                    ),
                                                    placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: CircularProgressIndicator()),
                                                    errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 50, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                  )),
                                            )),
                                        SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 16),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                SvgPicture.asset(
                                                  "assets/icon/profile.svg",
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 14,
                                                  height: GlobalVariable.ratioWidth(Get.context!) * 14,
                                                  color: Colors.black,
                                                ),
                                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                Container(
                                                    width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 120,
                                                    child: Text(
                                                      controller.dataPengurusBPD[x]['namapejabat'],
                                                      style: TextStyle(fontSize: AppFontSize.small, fontWeight: FontWeight.w600),
                                                    )),
                                              ]),
                                            ),
                                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 4),
                                            Container(
                                              child: Row(children: [
                                                SvgPicture.asset("assets/icon/whatsapp.svg", width: GlobalVariable.ratioWidth(Get.context!) * 14, height: GlobalVariable.ratioWidth(Get.context!) * 14),
                                                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 6),
                                                Text(controller.dataPengurusBPD[x]['telppejabat'], style: TextStyle(fontSize: AppFontSize.small, fontWeight: FontWeight.w400)),
                                              ]),
                                            )
                                          ],
                                        )
                                      ],
                                    ))),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          ]),
                        )
                    ])))
                  ],
                ))));
  }
}
