import 'package:alpha_bpd_apk/app/modules/bpd/controllers/bpd_controller.dart';
import 'package:get/get.dart';

class BPDBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(BPDController());
  }
}
