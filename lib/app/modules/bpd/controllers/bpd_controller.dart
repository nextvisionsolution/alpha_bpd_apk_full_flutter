import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_cli/common/utils/json_serialize/json_ast/utils/substring.dart';
import 'package:url_launcher/url_launcher.dart';

class BPDController extends GetxController {
  var loading = false.obs;
  var dataBPD;
  var dataPengurusBPD = [].obs;
  //TODO: Implement HomeController

  LocaleLanguage lang = LocaleLanguage();

  get dataMateri => null;
  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    await getProfilBPD();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getProfilBPD() async {
    var data = await ApiHelper().getProfilBPD();
    loading.value = false;
    if (data['status'] == 'OK') {
      dataBPD = data['data']['profil_bpd'];
      dataBPD = data['data']['profil_bpd'];

      for (var x = 0; x < data['data']['pengurus'].length; x++) {
        var detail = {
          'jabatanpejabat': data['data']['pengurus'][x]['JABATAN'] ?? "-",
          'gambarpejabat': data['data']['pengurus'][x]['GAMBARPEJABAT'] ?? "-",
          'namapejabat': data['data']['pengurus'][x]['NAMAJABATAN'] + ". " + data['data']['pengurus'][x]['NAMAPEJABAT'] ?? "-",
          'telppejabat': data['data']['pengurus'][x]['TELP'] ?? "-",
        };

        dataPengurusBPD.add(detail);
      }
    }
  }
}
