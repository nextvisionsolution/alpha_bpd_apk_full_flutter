import 'package:alpha_bpd_apk/app/modules/profil/controllers/profil_controller.dart';
import 'package:get/get.dart';

class ProfilBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ProfilController());
  }
}
