import 'dart:io';

import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/controllers/perpuluhan_iuran_controller.dart';
import 'package:alpha_bpd_apk/app/modules/profil/controllers/profil_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_cli/common/utils/json_serialize/json_ast/utils/substring.dart';
import 'package:intl/intl.dart';

import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilView extends GetView<ProfilController> {
  const ProfilView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          await controller.out();
          return false;
        },
        child: SafeArea(
            child: Scaffold(
                backgroundColor: Colors.white,
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
                  child: Container(
                      height: GlobalVariable.ratioWidth(Get.context!) * 56,
                      decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1))),
                      padding: EdgeInsets.symmetric(
                        vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
                        horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            child: SvgPicture.asset("assets/icon/chevron_left.svg", width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 30),
                            onTap: () {
                              controller.out();
                            },
                          ),
                          SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Expanded(
                            child: Text(
                              controller.title.value,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large),
                            ),
                          )
                        ],
                      )),
                ),
                body: Obx(() => controller.loading.value
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : SingleChildScrollView(
                        child: Column(
                        children: [
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Text(
                            controller.jenisTampilan.value == "GEREJA" ? LocaleLanguage().tr(LocaleKeys.LOGOGEREJA) : LocaleLanguage().tr(LocaleKeys.GAMBARPROFIL),
                            style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          GestureDetector(
                              onTap: controller.readOnly
                                  ? null
                                  : () async {
                                      controller.browseGambar("PROFIL");
                                    },
                              child: Container(
                                  width: GlobalVariable.ratioWidth(Get.context!) * 77.5,
                                  height: GlobalVariable.ratioWidth(Get.context!) * 75,
                                  decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: AppColors.border,
                                        blurRadius: 4,
                                        offset: Offset(0, 4), // Shadow position
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 70),
                                  ),
                                  child: Stack(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 70),
                                        child: Container(
                                            height: GlobalVariable.ratioWidth(Get.context!) * 75,
                                            width: GlobalVariable.ratioWidth(Get.context!) * 75,
                                            child: controller.ubahGambarProfil.value.path != ""
                                                ? Container(
                                                    height: GlobalVariable.ratioWidth(Get.context!) * 75,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(image: FileImage(controller.ubahGambarProfil.value), fit: BoxFit.cover),
                                                    ),
                                                  )
                                                : CachedNetworkImage(
                                                    imageUrl: controller.gambarProfil.value + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                    imageBuilder: (context, imageProvider) => Container(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 75,
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                                      ),
                                                    ),
                                                    placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 75, child: CircularProgressIndicator()),
                                                    errorWidget: (context, url, error) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 75, child: Center(child: Icon(Icons.hide_image_outlined))),
                                                  )),
                                      ),
                                      !controller.readOnly
                                          ? Positioned(child: Align(alignment: Alignment.bottomRight, child: SvgPicture.asset("assets/icon/camera.svg", color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 24)))
                                          : SizedBox()
                                    ],
                                  ))),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocaleLanguage().tr(LocaleKeys.NAMA),
                                style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                textAlign: TextAlign.left,
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: TextField(
                                readOnly: controller.readOnly,
                                controller: controller.namaController.value,
                                style: TextStyle(
                                  fontSize: AppFontSize.medium,
                                  fontWeight: FontWeight.w500,
                                ),
                                decoration: InputDecoration(
                                    filled: controller.readOnly,
                                    fillColor: controller.readOnly ? AppColors.backgroundDisabled : Colors.transparent,
                                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                    hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANNAMA),
                                    hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    )),
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocaleLanguage().tr(LocaleKeys.NOTELP),
                                style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                textAlign: TextAlign.left,
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: TextField(
                                    readOnly: controller.readOnly,
                                    controller: controller.telpController.value,
                                    keyboardType: TextInputType.phone,
                                    style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    decoration: InputDecoration(
                                        filled: controller.readOnly,
                                        fillColor: controller.readOnly ? AppColors.backgroundDisabled : Colors.transparent,
                                        contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                        hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANPHONE),
                                        hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                          borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                          borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                        )),
                                  )),
                                  controller.readOnly && controller.telpController.value.text != ""
                                      ? Row(
                                          children: [
                                            SizedBox(
                                              width: GlobalVariable.ratioWidth(Get.context!) * 10,
                                            ),
                                            controller.jenisTampilan.value == 'PEJABAT'
                                                ? GestureDetector(
                                                    onTap: () {
                                                      GlobalVariable.callingPopUp(controller.telpController.value.text);
                                                    },
                                                    child: SvgPicture.asset("assets/icon/whatsapp.svg", color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 32, height: GlobalVariable.ratioWidth(Get.context!) * 32),
                                                  )
                                                : GestureDetector(
                                                    onTap: () async {
                                                      String telp = controller.telpController.value.text;
                                                      if (telp[0] == "0") {
                                                        telp = "62" + substring(telp, 1, telp.length);
                                                      }

                                                      if (!await launchUrl(Uri.parse('tel://' + telp))) {
                                                        throw Exception('Could not launch');
                                                      }
                                                    },
                                                    child: SvgPicture.asset("assets/icon/phone.svg", color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 32, height: GlobalVariable.ratioWidth(Get.context!) * 32),
                                                  )
                                          ],
                                        )
                                      : SizedBox()
                                ],
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocaleLanguage().tr(LocaleKeys.NOFAX),
                                style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                textAlign: TextAlign.left,
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: TextField(
                                readOnly: controller.readOnly,
                                controller: controller.faxController.value,
                                keyboardType: TextInputType.phone,
                                style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                    filled: controller.readOnly,
                                    fillColor: controller.readOnly ? AppColors.backgroundDisabled : Colors.transparent,
                                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                    hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANFAX),
                                    hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    )),
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocaleLanguage().tr(LocaleKeys.EMAIL),
                                style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                textAlign: TextAlign.left,
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: TextField(
                                readOnly: controller.readOnly,
                                controller: controller.emailController.value,
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                    filled: controller.readOnly,
                                    fillColor: controller.readOnly ? AppColors.backgroundDisabled : Colors.transparent,
                                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                    hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANEMAIL),
                                    hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    )),
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocaleLanguage().tr(LocaleKeys.ALAMAT),
                                style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                textAlign: TextAlign.left,
                              )),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                              child: TextField(
                                readOnly: controller.readOnly,
                                controller: controller.alamatController.value,
                                style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                    filled: controller.readOnly,
                                    fillColor: controller.readOnly ? AppColors.backgroundDisabled : Colors.transparent,
                                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                    hintText: LocaleLanguage().tr(LocaleKeys.MASUKKANALAMAT),
                                    hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat),
                                      borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                    )),
                              )),
                          controller.jenisTampilan == "GEREJA"
                              ? Column(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16, vertical: GlobalVariable.ratioWidth(Get.context!) * 10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.GAMBARBANNER),
                                          style: TextStyle(fontWeight: FontWeight.w600, fontSize: GlobalVariable.ratioWidth(Get.context!) * 14),
                                          textAlign: TextAlign.left,
                                        )),
                                    GestureDetector(
                                        onTap: controller.readOnly
                                            ? null
                                            : () async {
                                                controller.browseGambar("BANNER");
                                              },
                                        child: Container(
                                            width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                            height: GlobalVariable.ratioWidth(Get.context!) * 168,
                                            decoration: BoxDecoration(color: Colors.white, border: Border.all(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: AppColors.borderPejabat), borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10)),
                                            child: controller.ubahGambarBanner.value.path != ""
                                                ? ClipRRect(
                                                    borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 10)),
                                                    child: Container(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 168,
                                                      width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(image: FileImage(controller.ubahGambarBanner.value), fit: BoxFit.cover),
                                                      ),
                                                    ))
                                                : CachedNetworkImage(
                                                    imageUrl: controller.gambarBanner.value + "?t=" + DateFormat('kkmmss').format(DateTime.now()),
                                                    imageBuilder: (context, imageProvider) => ClipRRect(
                                                        borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 10)),
                                                        child: Container(
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 168,
                                                          width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                                          decoration: BoxDecoration(
                                                            image: DecorationImage(
                                                              image: imageProvider,
                                                              fit: BoxFit.cover,
                                                            ),
                                                          ),
                                                        )),
                                                    placeholder: (context, url) => Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 168, child: CircularProgressIndicator()),
                                                    errorWidget: (context, url, error) => ClipRRect(
                                                        borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 10)),
                                                        child: Container(color: Colors.white, height: GlobalVariable.ratioWidth(Get.context!) * 168, width: GlobalVariable.ratioWidth(Get.context!) * 122, child: Icon(Icons.hide_image_outlined))),
                                                  ))),
                                  ],
                                )
                              : SizedBox(),
                          !controller.readOnly
                              ? Container(
                                  margin: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 10),
                                  width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                  child: ElevatedButton(
                                      onPressed: () {
                                        controller.simpanProfil();
                                      },
                                      child: Text(
                                        LocaleLanguage().tr(LocaleKeys.SIMPAN),
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                                      ),
                                      style: ButtonStyle(
                                          elevation: MaterialStateProperty.all<double>(0),
                                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 12)),
                                          backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor))))))
                              : SizedBox(),
                          controller.jenisTampilan == "PEJABAT"
                              ? Container(
                                  margin: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 10),
                                  width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                  child: ElevatedButton(
                                      onPressed: () async {
                                        GetToPage.toNamed<PerpuluhanIuranController>(Routes.PERPULUHAN_IURAN, arguments: ["IURAN", controller.id, "GBI " + controller.namaGereja, controller.namaPejabat]);
                                      },
                                      child: Text(
                                        LocaleLanguage().tr(LocaleKeys.IURAN),
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                      ),
                                      style: ButtonStyle(
                                          elevation: MaterialStateProperty.all<double>(0),
                                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                          backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1))))))
                              : SizedBox(),
                          controller.jenisTampilan == "PEJABAT" && !controller.readOnly
                              ? Container(
                                  margin: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 10),
                                  width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                  child: ElevatedButton(
                                      onPressed: () async {
                                        controller.popUpPassword();
                                      },
                                      child: Text(
                                        LocaleLanguage().tr(LocaleKeys.GANTIPASSWORD),
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                      ),
                                      style: ButtonStyle(
                                          elevation: MaterialStateProperty.all<double>(0),
                                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                          backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1))))))
                              : SizedBox(),
                          SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10)
                        ],
                      ))))));
  }
}
