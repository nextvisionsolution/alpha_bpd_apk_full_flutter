import 'dart:io';

import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ProfilController extends GetxController {
  //TODO: Implement HomeController
  var jenisTampilan = "".obs;
  var id = "0";
  var namaGereja = "";
  var namaPejabat = "";
  var title = "".obs;
  var dataProfil = {}.obs;
  var loading = false.obs;
  var namaController = TextEditingController(text: '').obs;
  var telpController = TextEditingController(text: '').obs;
  var faxController = TextEditingController(text: '').obs;
  var emailController = TextEditingController(text: '').obs;
  var alamatController = TextEditingController(text: '').obs;
  var newpasswordController = TextEditingController(text: '').obs;
  var cnewpasswordController = TextEditingController(text: '').obs;
  var oldpasswordController = TextEditingController(text: '').obs;
  var gambarBanner = "".obs;
  var gambarProfil = "".obs;
  var ubahGambarBanner = File("").obs;
  var ubahGambarProfil = File("").obs;
  var perubahan = false;
  var errorPasswordLama = ''.obs;
  var errorPasswordBaru = ''.obs;
  var errorKonfirmasiPasswordBaru = ''.obs;

  var readOnly = false;
  var obscureOldPassword = true.obs;
  var obscureNewPassword = true.obs;
  var obscureCNewPassword = true.obs;

  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
    jenisTampilan.value = Get.arguments[0];
    id = Get.arguments[1];
    namaGereja = Get.arguments[2];
    namaPejabat = Get.arguments[3];

    if (jenisTampilan.value == "GEREJA" && GlobalVariable.statusGembala) {
      readOnly = false;
      title.value = LocaleLanguage().tr(LocaleKeys.UBAHPROFILGEREJA);
    } else if (jenisTampilan.value == "GEREJA" && !GlobalVariable.statusGembala) {
      readOnly = true;
      title.value = LocaleLanguage().tr(LocaleKeys.PROFILGEREJA);
    } else if (jenisTampilan.value == "PEJABAT" && id == GlobalVariable.idPejabat) {
      readOnly = false;
      title.value = LocaleLanguage().tr(LocaleKeys.UBAHPROFIL);
    } else if (jenisTampilan.value == "PEJABAT" && id != GlobalVariable.idPejabat) {
      readOnly = true;
      title.value = LocaleLanguage().tr(LocaleKeys.PROFIL);
    }

    getDataProfil();
  }

  getDataProfil() async {
    loading.value = true;
    ubahGambarBanner.value = File("");
    ubahGambarProfil.value = File("");

    if (jenisTampilan.value == "GEREJA") {
      var data = await ApiHelper().getProfilGereja(id);
      if (data['status'] == 'OK') {
        dataProfil.value = data['data'];

        namaController.value.text = dataProfil['NAMAGEREJA'];
        telpController.value.text = dataProfil['TELP'];
        emailController.value.text = dataProfil['EMAIL'];
        faxController.value.text = dataProfil['FAX'];
        alamatController.value.text = dataProfil['ALAMATGEREJA'];
        gambarBanner.value = dataProfil['GAMBARBANNER'];
        gambarProfil.value = dataProfil['GAMBAR'];
      }
    } else if (jenisTampilan.value == "PEJABAT") {
      var data = await ApiHelper().getProfilPejabat(id);
      if (data['status'] == 'OK') {
        dataProfil.value = data['data'];
        namaController.value.text = dataProfil['NAMAPEJABAT'];
        telpController.value.text = dataProfil['TELP'];
        emailController.value.text = dataProfil['EMAIL'];
        faxController.value.text = dataProfil['FAX'];
        alamatController.value.text = dataProfil['ALAMAT'];
        gambarBanner.value = "";
        gambarProfil.value = dataProfil['GAMBARPEJABAT'];
      }
    }
    print(dataProfil.value);

    loading.value = false;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  browseGambar(jenis) {
    final ImagePicker picker = ImagePicker();

    GlobalVariable.picturePopUp(() async {
      Get.back();
      XFile? imageFileX = await picker.pickImage(source: ImageSource.camera);
      if (imageFileX != null) {
        File? file = File(imageFileX!.path);
        final croppedImage = await ImageCropper().cropImage(sourcePath: file.path);
        if (croppedImage != null) {
          if (jenis == "PROFIL") {
            ubahGambarProfil.value = File(croppedImage.path);
          } else {
            ubahGambarBanner.value = File(croppedImage.path);
          }
        }
      }
    }, () async {
      Get.back();
      XFile? imageFileX = await picker.pickImage(source: ImageSource.gallery);
      if (imageFileX != null) {
        File? file = File(imageFileX!.path);
        final croppedImage = await ImageCropper().cropImage(sourcePath: file.path);
        if (croppedImage != null) {
          if (jenis == "PROFIL") {
            ubahGambarProfil.value = File(croppedImage.path);
          } else {
            ubahGambarBanner.value = File(croppedImage.path);
          }
        }
      }
    });
  }

  simpanProfil() async {
    GlobalVariable.loadingPopUp(LocaleKeys.LOADING);
    var data;
    if (jenisTampilan.value == "GEREJA") {
      data =
          await ApiHelper().simpanProfilGereja(id, namaController.value.text, emailController.value.text, telpController.value.text, faxController.value.text, alamatController.value.text, GlobalVariable.convertToBase64(ubahGambarProfil.value), GlobalVariable.convertToBase64(ubahGambarBanner.value));
    } else {
      data = await ApiHelper().simpanProfilPejabat(id, namaController.value.text, emailController.value.text, telpController.value.text, faxController.value.text, alamatController.value.text, GlobalVariable.convertToBase64(ubahGambarProfil.value));
    }
    print(data);
    Get.back();
    if (data['status'] == 'OK') {
      GlobalVariable.successPopUp("");
      perubahan = true;
      getDataProfil();
    } else {
      GlobalVariable.errorPopUp("");
    }
  }

  out() async {
    Get.back(result: perubahan);
  }

  void popUpPassword() async {
    newpasswordController.value.clear();
    cnewpasswordController.value.clear();
    oldpasswordController.value.clear();
    obscureOldPassword.value = true;
    obscureNewPassword.value = true;
    obscureCNewPassword.value = true;
    errorPasswordLama.value = "";
    errorPasswordBaru.value = "";
    errorKonfirmasiPasswordBaru.value = "";
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                    contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 12, vertical: GlobalVariable.ratioWidth(Get.context!) * 16),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 8))),
                    content: Container(
                        width: MediaQuery.of(Get.context!).size.width,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Container(width: GlobalVariable.ratioWidth(Get.context!) * 24, child: Text("")),
                                Expanded(
                                  child: Text(""),
                                ),
                                Text(LocaleLanguage().tr(LocaleKeys.GANTIPASSWORD), textAlign: TextAlign.center, style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                                Expanded(
                                  child: Text(""),
                                ),
                                GestureDetector(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                        width: GlobalVariable.ratioWidth(Get.context!) * 24,
                                        child: Icon(
                                          Icons.close,
                                          size: GlobalVariable.ratioWidth(Get.context!) * 20,
                                        )))
                              ],
                            ),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Text(LocaleLanguage().tr(LocaleKeys.PASSWORDLAMA), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => TextField(
                                  controller: oldpasswordController.value,
                                  obscureText: obscureOldPassword.value,
                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                      suffixIcon: Container(
                                          padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                          child: GestureDetector(
                                              onTap: () {
                                                obscureOldPassword.value = !obscureOldPassword.value;
                                              },
                                              child: SvgPicture.asset(obscureOldPassword.value ? "assets/icon/eye_slash.svg" : "assets/icon/eye.svg"))),
                                      contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                      hintText: "",
                                      hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorPasswordLama.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      ),
                                      // Set border for focused state
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorPasswordLama.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      )),
                                )),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => errorPasswordLama.value != ""
                                ? Column(
                                    children: [
                                      Text(
                                        errorPasswordLama.value,
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12, fontWeight: FontWeight.w400, color: AppColors.menuOrange),
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                    ],
                                  )
                                : SizedBox()),
                            Text(LocaleLanguage().tr(LocaleKeys.PASSWORDBARU), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => TextField(
                                  controller: newpasswordController.value,
                                  obscureText: obscureNewPassword.value,
                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                      suffixIcon: Container(
                                          padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                          child: GestureDetector(
                                              onTap: () {
                                                obscureNewPassword.value = !obscureNewPassword.value;
                                              },
                                              child: SvgPicture.asset(obscureNewPassword.value ? "assets/icon/eye_slash.svg" : "assets/icon/eye.svg"))),
                                      contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                      hintText: "",
                                      hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorPasswordBaru.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      ),
                                      // Set border for focused state
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorPasswordBaru.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      )),
                                )),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => errorPasswordBaru.value != ""
                                ? Column(
                                    children: [
                                      Text(
                                        errorPasswordBaru.value,
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12, fontWeight: FontWeight.w400, color: AppColors.menuOrange),
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                    ],
                                  )
                                : SizedBox()),
                            Text(LocaleLanguage().tr(LocaleKeys.KONFIRMASIPASSWORDBARU), style: TextStyle(fontSize: GlobalVariable.ratioWidth(Get.context!) * 14, fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => TextField(
                                  controller: cnewpasswordController.value,
                                  obscureText: obscureCNewPassword.value,
                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                      suffixIcon: Container(
                                          padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                          child: GestureDetector(
                                              onTap: () {
                                                obscureCNewPassword.value = !obscureCNewPassword.value;
                                              },
                                              child: SvgPicture.asset(
                                                obscureCNewPassword.value ? "assets/icon/eye_slash.svg" : "assets/icon/eye.svg",
                                              ))),
                                      contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
                                      hintText: "",
                                      hintStyle: TextStyle(color: AppColors.placeholderColor, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorKonfirmasiPasswordBaru.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      ),
                                      // Set border for focused state
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(width: GlobalVariable.ratioWidth(Get.context!) * 1, color: errorKonfirmasiPasswordBaru.value != "" ? AppColors.menuOrange : AppColors.borderPejabat),
                                        borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8),
                                      )),
                                )),
                            SizedBox(
                              height: GlobalVariable.ratioWidth(Get.context!) * 10,
                            ),
                            Obx(() => errorKonfirmasiPasswordBaru.value != ""
                                ? Column(
                                    children: [
                                      Text(
                                        errorKonfirmasiPasswordBaru.value,
                                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 12, fontWeight: FontWeight.w400, color: AppColors.menuOrange),
                                      ),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                    ],
                                  )
                                : SizedBox()),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.BATAL),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700, color: AppColors.goldColor),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1)))))),
                                Container(
                                    width: GlobalVariable.ratioWidth(Get.context!) * 148,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          //Get.back();
                                          setPassword();
                                        },
                                        child: Text(
                                          LocaleLanguage().tr(LocaleKeys.SIMPAN),
                                          style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(0),
                                            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 14)),
                                            backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1))))))
                              ],
                            )
                          ],
                        )))),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: Get.context!,
        pageBuilder: (context, animation1, animation2) {
          return Text("");
        });
  }

  setPassword() async {
    var valid = true;

    errorPasswordLama.value = "";
    errorPasswordBaru.value = "";
    errorKonfirmasiPasswordBaru.value = "";

    if (oldpasswordController.value.text == "") {
      valid = false;
      errorPasswordLama.value = LocaleLanguage().tr(LocaleKeys.ERRORPASSWORDLAMA);
    }
    if (newpasswordController.value.text == "") {
      valid = false;
      errorPasswordBaru.value = LocaleLanguage().tr(LocaleKeys.ERRORPASSWORDBARU);
    }
    if (cnewpasswordController.value.text == "" || newpasswordController.value.text != cnewpasswordController.value.text) {
      valid = false;

      errorKonfirmasiPasswordBaru.value = LocaleLanguage().tr(LocaleKeys.ERRORKONFIRMPASSWORDBARU);
    }

    if (valid) {
      GlobalVariable.loadingPopUp(LocaleKeys.LOADING);
      var data = await ApiHelper().gantiPassword(id, oldpasswordController.value.text, newpasswordController.value.text);

      Get.back();
      if (data['status'] == 'OK') {
        Get.back();
        GlobalVariable.successPopUp("");
        perubahan = true;
        getDataProfil();
      } else {
        errorPasswordLama.value = LocaleLanguage().tr(LocaleKeys.ERRORPASSWORDLAMATIDAKSESUAI);
      }
    }
  }
}
