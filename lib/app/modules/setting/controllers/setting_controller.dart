import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/login/controllers/login_controller.dart';
import 'package:alpha_bpd_apk/app/modules/splash/view/splash_view.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingController extends GetxController {
  var loading = false.obs;
  //TODO: Implement HomeController

  LocaleLanguage lang = LocaleLanguage();
  var bahasa = {
    'id': 'Indonesia',
    'en': 'English',
  }.obs;

  var keyBahasa = ''.obs;

  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    keyBahasa.value = await SharedPreferencesHelper.getCodeLanguage();
    loading.value = false;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void gantiBahasa() {
    var lastKey = "";
    for (var key in bahasa.keys) {
      lastKey = key;
    }
    showModalBottomSheet(
        context: Get.context!,
        backgroundColor: Colors.transparent,
        builder: (context) => Container(
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16), topRight: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16))),
              padding: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 8, bottom: GlobalVariable.ratioWidth(Get.context!) * 8, left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: GlobalVariable.ratioWidth(Get.context!) * 60,
                    height: GlobalVariable.ratioWidth(Get.context!) * 6,
                    decoration: BoxDecoration(color: AppColors.goldColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 3)),
                  ),
                  SizedBox(
                    height: GlobalVariable.ratioWidth(Get.context!) * 10,
                  ),
                  for (var key in bahasa.keys)
                    GestureDetector(
                        onTap: () async {
                          Get.back();
                          if (keyBahasa.value != key) {
                            GlobalVariable.confirmPopUp(LocaleLanguage().tr(LocaleKeys.UBAHBAHASAQUESTION), () {
                              Get.back();
                            }, () async {
                              await SharedPreferencesHelper.setCodeLanguage(key);
                              keyBahasa.value = await SharedPreferencesHelper.getCodeLanguage();

                              print(keyBahasa.value);

                              Get.back();
                              Get.offNamedUntil(Routes.SPLASH, (route) => false);
                            });
                          }
                        },
                        child: Container(
                          child: Column(children: [
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            Container(
                                width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                child: Text(
                                  bahasa[key]!,
                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                )),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            key != lastKey
                                ? Container(
                                    width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 1,
                                    decoration: BoxDecoration(color: AppColors.separatorColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 3)),
                                  )
                                : SizedBox(),
                          ]),
                        ))
                ],
              ),
            ));
  }
}
