import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/login/controllers/login_controller.dart';
import 'package:alpha_bpd_apk/app/modules/setting/controllers/setting_controller.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

class SettingView extends GetView<SettingController> {
  const SettingView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
        child: Container(
            height: GlobalVariable.ratioWidth(Get.context!) * 56,
            decoration: BoxDecoration(color: Colors.white, boxShadow: <BoxShadow>[BoxShadow(color: AppColors.border, blurRadius: 0, spreadRadius: GlobalVariable.ratioWidth(Get.context!) * 1)]),
            padding: EdgeInsets.symmetric(
              vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
              horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
            ),
            child: Row(
              children: [
                GestureDetector(
                  child: SvgPicture.asset("assets/icon/chevron_left.svg", width: GlobalVariable.ratioWidth(Get.context!) * 30, height: GlobalVariable.ratioWidth(Get.context!) * 30),
                  onTap: () {
                    Get.back();
                  },
                ),
                SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                Expanded(
                  child: Text(
                    LocaleLanguage().tr(LocaleKeys.SETTING),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: AppFontSize.large),
                  ),
                ),
              ],
            )),
      ),
      body: Obx(() => controller.loading.value
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: GlobalVariable.ratioWidth(Get.context!) * 10,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                  child: Text(
                    LocaleLanguage().tr(LocaleKeys.LANGUAGE),
                    style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w600),
                  )),
              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
              GestureDetector(
                  onTap: () {
                    controller.gantiBahasa();
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                      margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                      padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 15),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 8), border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Obx(() => Text(
                                controller.bahasa[controller.keyBahasa.value]!,
                                style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 14, fontWeight: FontWeight.w500),
                              )),
                          SvgPicture.asset("assets/icon/chevron_down.svg", color: Colors.black, width: GlobalVariable.ratioFontSize(Get.context!) * 20, height: GlobalVariable.ratioFontSize(Get.context!) * 20)
                        ],
                      ))),
              SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 14),
              Container(
                  width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                  margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                  child: ElevatedButton(
                      onPressed: () {
                        GlobalVariable.logout();
                      },
                      child: Text(
                        LocaleLanguage().tr(LocaleKeys.LOGOUT),
                        style: TextStyle(fontSize: GlobalVariable.ratioFontSize(Get.context!) * 16, fontWeight: FontWeight.w700),
                      ),
                      style: ButtonStyle(
                          elevation: MaterialStateProperty.all<double>(0),
                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 12)),
                          backgroundColor: MaterialStateProperty.all<Color>(AppColors.goldColor),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 12), side: BorderSide(color: AppColors.goldColor))))))
            ]))),
    ));
  }
}
