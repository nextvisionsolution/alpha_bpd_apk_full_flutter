import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/controllers/perpuluhan_iuran_controller.dart';
import 'package:get/get.dart';

class PerpuluhanIuranBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PerpuluhanIuranController());
  }
}
