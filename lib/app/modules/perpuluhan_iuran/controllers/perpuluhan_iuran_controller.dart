import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PerpuluhanIuranController extends GetxController {
  var loading = false.obs;
  var loadingPerpuluhanIuran = false.obs;
  var jenisLaporan = ''.obs;
  var arrTahun = [].obs;
  var arrTahunTampil;
  var dataPerpuluhanIuran = [].obs;
  var indexSelected = 0.obs;
  var id = "";
  var namaGereja = "";
  var namaPejabat = "";
  var total = 0.0.obs;
  List<grafik> point = [];
  //TODO: Implement HomeController

  LocaleLanguage lang = LocaleLanguage();

  get dataMateri => null;
  @override
  void onInit() async {
    super.onInit();
    loading.value = true;
    loadingPerpuluhanIuran.value = true;

    jenisLaporan.value = Get.arguments[0];
    id = Get.arguments[1].toString();
    namaGereja = Get.arguments[2].toString();
    namaPejabat = Get.arguments[3].toString();

    await getTahunPerpuluhanIuran(id);
    if (arrTahun.length > 0) {
      await getDataPerpuluhanIuran(id, arrTahun[indexSelected.value]);
    } else {
      loadingPerpuluhanIuran.value = false;
    }
    dataPerpuluhanIuran.refresh();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getTahunPerpuluhanIuran(id) async {
    if (jenisLaporan.value == "PERPULUHAN") {
      var data = await ApiHelper().getTahunPerpuluhan(id);
      if (data['status'] == 'OK') {
        for (var x = 0; x < data['data'].length; x++) {
          arrTahun.add(data['data'][x]);
        }
      }
    } else {
      var data = await ApiHelper().getTahunIuran(id);
      if (data['status'] == 'OK') {
        for (var x = 0; x < data['data'].length; x++) {
          arrTahun.add(data['data'][x]);
        }
      }
    }

    loading.value = false;
    if (arrTahun.length > 0) {
      arrTahunTampil = arrTahun.where((element) => element != arrTahun[indexSelected.value]).toList();
    }
  }

  getDataPerpuluhanIuran(id, tahun) async {
    dataPerpuluhanIuran.clear();
    point.clear();
    loadingPerpuluhanIuran.value = true;
    total.value = 0;
    if (jenisLaporan.value == "PERPULUHAN") {
      var data = await ApiHelper().getDataPerpuluhan(id, tahun);
      if (data['status'] == 'OK') {
        for (var x = data['data']['detail'].length - 1; x >= 0; x--) {
          dataPerpuluhanIuran.add({
            'bulan': GlobalVariable.cekBulan(data['data']['detail'][x]['BULAN']) + " " + data['data']['detail'][x]['TAHUN'],
            'tanggal': GlobalVariable.formatDate(data['data']['detail'][x]['TGLTRANS']),
            'amount': data['data']['detail'][x]['AMOUNT'] ?? "0",
          });
          total.value += double.parse(data['data']['detail'][x]['AMOUNT']);
          point.add(grafik(
            GlobalVariable.cekBulan(data['data']['detail'][x]['BULAN']),
            double.parse(data['data']['detail'][x]['AMOUNT']),
          ));
        }
      }
    } else {
      var data = await ApiHelper().getDataIuran(id, tahun);
      if (data['status'] == 'OK') {
        for (var x = data['data']['detail'].length - 1; x >= 0; x--) {
          dataPerpuluhanIuran.add({
            'bulan': GlobalVariable.cekBulan(data['data']['detail'][x]['BULAN']) + " " + data['data']['detail'][x]['TAHUN'],
            'tanggal': GlobalVariable.formatDate(data['data']['detail'][x]['TGLTRANS']),
            'amount': data['data']['detail'][x]['AMOUNT'] ?? "0",
          });
          total.value += double.parse(data['data']['detail'][x]['AMOUNT']);
        }
      }
    }

    loadingPerpuluhanIuran.value = false;
    dataPerpuluhanIuran.refresh();
  }

  void ubahTahun() async {
    showModalBottomSheet(
        context: Get.context!,
        backgroundColor: Colors.transparent,
        builder: (context) => Container(
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16), topRight: Radius.circular(GlobalVariable.ratioWidth(Get.context!) * 16))),
              padding: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 8, bottom: GlobalVariable.ratioWidth(Get.context!) * 8, left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: GlobalVariable.ratioWidth(Get.context!) * 60,
                    height: GlobalVariable.ratioWidth(Get.context!) * 6,
                    decoration: BoxDecoration(color: AppColors.goldColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 3)),
                  ),
                  SizedBox(
                    height: GlobalVariable.ratioWidth(Get.context!) * 10,
                  ),
                  for (var x = 0; x < arrTahun.length; x++)
                    GestureDetector(
                        onTap: () async {
                          indexSelected.value = x;
                          arrTahunTampil = arrTahun.where((element) => element != arrTahun[indexSelected.value]).toList();
                          arrTahun.refresh();
                          Get.back();
                          await getDataPerpuluhanIuran(id, arrTahun[indexSelected.value]);
                        },
                        child: Container(
                          child: Column(children: [
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            Container(
                                width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                child: Text(
                                  arrTahun[x]!,
                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
                                )),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            x != arrTahun.length - 1
                                ? Container(
                                    width: MediaQuery.of(context).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                    height: GlobalVariable.ratioWidth(Get.context!) * 1,
                                    decoration: BoxDecoration(color: AppColors.separatorColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 3)),
                                  )
                                : SizedBox(),
                          ]),
                        ))
                ],
              ),
            ));
  }
}

class grafik {
  grafik(this.year, this.sales);

  final String year;
  final double sales;
}
