import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/controllers/perpuluhan_iuran_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:url_launcher/url_launcher.dart';

class PerpuluhanIuranView extends GetView<PerpuluhanIuranController> {
  const PerpuluhanIuranView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Obx(() => controller.loading.value
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Scaffold(
                backgroundColor: Colors.white,
                // appBar: PreferredSize(
                //   preferredSize: Size.fromHeight(GlobalVariable.ratioWidth(Get.context!) * 56),
                //   child: Container(
                //       height: GlobalVariable.ratioWidth(Get.context!) * 56,
                //       decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1))),
                //       padding: EdgeInsets.symmetric(
                //         vertical: GlobalVariable.ratioWidth(Get.context!) * 12,
                //         horizontal: GlobalVariable.ratioWidth(Get.context!) * 16,
                //       ),
                //       child: ),
                // ),
                body: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                      child: Stack(
                    children: [
                      Container(
                          width: MediaQuery.of(Get.context!).size.width,
                          height: GlobalVariable.ratioWidth(Get.context!) * 150,
                          child: Image.asset(
                            'assets/images/banner.jpg',
                            fit: BoxFit.cover,
                          )),
                      Positioned(
                          left: GlobalVariable.ratioWidth(Get.context!) * 16,
                          top: GlobalVariable.ratioWidth(Get.context!) * 12,
                          child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                  child: Row(
                                children: [
                                  GestureDetector(
                                    child: SvgPicture.asset(
                                      "assets/icon/chevron_left.svg",
                                      width: GlobalVariable.ratioWidth(Get.context!) * 30,
                                      height: GlobalVariable.ratioWidth(Get.context!) * 30,
                                      color: Colors.white,
                                    ),
                                    onTap: () {
                                      Get.back();
                                    },
                                  ),
                                  SizedBox(width: GlobalVariable.ratioWidth(Get.context!) * 10),
                                  Text(
                                    (controller.jenisLaporan.value == "PERPULUHAN" ? LocaleLanguage().tr(LocaleKeys.PERPULUHAN) : LocaleLanguage().tr(LocaleKeys.IURAN)),
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: AppFontSize.large,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              )))),
                      Positioned(
                          right: GlobalVariable.ratioWidth(Get.context!) * 16,
                          bottom: GlobalVariable.ratioWidth(Get.context!) * 10,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Row(children: [
                              SizedBox(
                                  width: MediaQuery.of(Get.context!).size.width - GlobalVariable.ratioWidth(Get.context!) * 32,
                                  child: Obx(() => Text(LocaleLanguage().tr(LocaleKeys.THANKYOUFORYOURLOYALTY) + "\n" + (controller.jenisLaporan.value == 'PERPULUHAN' ? controller.namaGereja : controller.namaPejabat),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.right,
                                      maxLines: 3,
                                      style: TextStyle(
                                        fontSize: AppFontSize.large,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      )))),
                            ]),
                          )),
                    ],
                  )),
                  Container(
                      child: Column(children: [
                    controller.arrTahun.length == 0
                        ? Container(
                            height: GlobalVariable.ratioWidth(Get.context!) * 500,
                            child: Center(
                                child: Text(
                              LocaleLanguage().tr(LocaleKeys.TIDAKADADATA),
                              style: TextStyle(fontSize: AppFontSize.large),
                            )))
                        : Container(
                            child: Column(
                            children: [
                              Container(
                                  color: Colors.white,
                                  padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16, vertical: GlobalVariable.ratioWidth(Get.context!) * 14),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            print(controller.arrTahunTampil.length);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: GlobalVariable.ratioWidth(Get.context!) * 16),
                                            alignment: Alignment.center,
                                            width: GlobalVariable.ratioWidth(Get.context!) * 84,
                                            decoration: BoxDecoration(color: AppColors.goldColor, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 20), border: Border.all(color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                                            padding: EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 5),
                                            child: Text(
                                              controller.arrTahun[controller.indexSelected.value],
                                              style: TextStyle(color: Colors.white, fontSize: AppFontSize.medium),
                                            ),
                                          )),
                                      for (var x = 0; x < controller.arrTahunTampil.length; x++)
                                        x < 2
                                            ? GestureDetector(
                                                onTap: () async {
                                                  for (var y = 0; y < controller.arrTahun.length; y++) {
                                                    if (controller.arrTahun[y] == controller.arrTahunTampil[x]) {
                                                      controller.indexSelected.value = y;
                                                    }
                                                  }

                                                  print(controller.indexSelected.value);
                                                  controller.arrTahunTampil = controller.arrTahun.where((element) => element != controller.arrTahun[controller.indexSelected.value]).toList();

                                                  print(controller.arrTahunTampil);

                                                  controller.arrTahun.refresh();

                                                  await controller.getDataPerpuluhanIuran(controller.id, controller.arrTahun[controller.indexSelected.value]);
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(right: GlobalVariable.ratioWidth(Get.context!) * 16),
                                                  alignment: Alignment.center,
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 84,
                                                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 20), border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                                                  padding: EdgeInsets.symmetric(vertical: GlobalVariable.ratioWidth(Get.context!) * 5),
                                                  child: Text(
                                                    controller.arrTahunTampil[x],
                                                    style: TextStyle(color: Colors.black, fontSize: AppFontSize.medium),
                                                  ),
                                                ))
                                            : SizedBox(),
                                      controller.arrTahunTampil.length > 3
                                          ? GestureDetector(
                                              onTap: () {
                                                controller.ubahTahun();
                                              },
                                              child: SvgPicture.asset("assets/icon/filter.svg", color: AppColors.goldColor, width: GlobalVariable.ratioWidth(Get.context!) * 24, height: GlobalVariable.ratioWidth(Get.context!) * 24))
                                          : SizedBox()
                                    ],
                                  )),
                            ],
                          ))
                  ])),
                  controller.arrTahun.length == 0
                      ? SizedBox()
                      : Expanded(
                          child: SingleChildScrollView(
                              child: controller.loadingPerpuluhanIuran.value
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      controller.jenisLaporan.value == "PERPULUHAN" ? SizedBox() : SizedBox(),
                                      controller.jenisLaporan.value == "PERPULUHAN"
                                          ? Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                              Container(
                                                margin: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                                                child: Text(
                                                  LocaleLanguage().tr(LocaleKeys.GRAFIKPERPULUHAN) + " " + controller.arrTahun[controller.indexSelected.value],
                                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w700),
                                                ),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16, top: GlobalVariable.ratioWidth(Get.context!) * 10, bottom: GlobalVariable.ratioWidth(Get.context!) * 3),
                                                  width: 1000,
                                                  child: Column(children: [
                                                    //Initialize the chart widget
                                                    SfCartesianChart(
                                                        primaryXAxis: CategoryAxis(),
                                                        primaryYAxis: NumericAxis(
                                                          rangePadding: ChartRangePadding.round,
                                                          anchorRangeToVisiblePoints: true,
                                                        ),
                                                        // Enable legend
                                                        legend: Legend(isVisible: false),
                                                        // Enable tooltip
                                                        tooltipBehavior: TooltipBehavior(enable: true),
                                                        series: <ChartSeries<grafik, String>>[
                                                          LineSeries<grafik, String>(
                                                              markerSettings: MarkerSettings(shape: DataMarkerType.circle, borderWidth: 3, borderColor: Colors.black),
                                                              dataSource: controller.point,
                                                              xValueMapper: (grafik sales, _) => sales.year,
                                                              yValueMapper: (grafik sales, _) => sales.sales,
                                                              name: LocaleLanguage().tr(LocaleKeys.PERPULUHAN),
                                                              // Enable data label
                                                              dataLabelSettings: DataLabelSettings(isVisible: true))
                                                        ]),
                                                  ]))
                                            ])
                                          : SizedBox(),
                                      Container(
                                          color: Colors.black,
                                          width: MediaQuery.of(Get.context!).size.width,
                                          padding: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 26, top: GlobalVariable.ratioWidth(Get.context!) * 8, bottom: GlobalVariable.ratioWidth(Get.context!) * 8),
                                          child: Text(
                                            LocaleLanguage().tr(LocaleKeys.TOTAL) + " Rp" + GlobalVariable.formatCurrencyDecimal(controller.total.value.toString()),
                                            style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w700, color: Colors.white),
                                            textAlign: TextAlign.right,
                                          )),
                                      SizedBox(
                                        height: GlobalVariable.ratioWidth(Get.context!) * 10,
                                      ),
                                      for (var x = 0; x < controller.dataPerpuluhanIuran.length; x++)
                                        Container(
                                            margin: EdgeInsets.only(left: GlobalVariable.ratioWidth(Get.context!) * 16, right: GlobalVariable.ratioWidth(Get.context!) * 16, bottom: GlobalVariable.ratioWidth(Get.context!) * 10),
                                            padding: EdgeInsets.all(GlobalVariable.ratioWidth(Get.context!) * 10),
                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(GlobalVariable.ratioWidth(Get.context!) * 10), border: Border.all(color: AppColors.borderPejabat, width: GlobalVariable.ratioWidth(Get.context!) * 1)),
                                            child: Row(
                                              children: [
                                                GestureDetector(
                                                    onTap: () async {
                                                      if (!await launchUrl(Uri.parse(controller.dataPerpuluhanIuran[x]['LINKTANDATERIMA']))) {
                                                        throw Exception('Could not launch');
                                                      }
                                                    },
                                                    child: Container(
                                                        padding: EdgeInsets.all(GlobalVariable.ratioWidth(Get.context!) * 5),
                                                        child: SvgPicture.asset(
                                                          'assets/icon/download.svg',
                                                          width: GlobalVariable.ratioWidth(Get.context!) * 24,
                                                          height: GlobalVariable.ratioWidth(Get.context!) * 24,
                                                          color: AppColors.goldColor,
                                                        ))),
                                                SizedBox(
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 6,
                                                ),
                                                Container(
                                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                    Text(controller.dataPerpuluhanIuran[x]['bulan'], style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500)),
                                                    SizedBox(
                                                      height: GlobalVariable.ratioWidth(Get.context!) * 4,
                                                    ),
                                                    Text(controller.dataPerpuluhanIuran[x]['tanggal'], style: TextStyle(fontSize: AppFontSize.small, fontWeight: FontWeight.w400))
                                                  ]),
                                                ),
                                                Expanded(
                                                  child: Text(''),
                                                ),
                                                SizedBox(
                                                  width: GlobalVariable.ratioWidth(Get.context!) * 6,
                                                ),
                                                Text(
                                                  'Rp' + GlobalVariable.formatCurrencyDecimal(controller.dataPerpuluhanIuran[x]['amount']),
                                                  style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w700),
                                                )
                                              ],
                                            ))
                                    ])))
                ]))));
  }
}
