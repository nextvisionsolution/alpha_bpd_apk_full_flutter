import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/get_to_page_function.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/function/shared_preferences_helper.dart';
import 'package:alpha_bpd_apk/app/modules/home/controllers/home_controller.dart';
import 'package:alpha_bpd_apk/app/routes/app_pages.dart';
import 'package:toastification/toastification.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cool_alert/cool_alert.dart';

class LoginController extends GetxController {
  var userController = TextEditingController(text: '');
  var passController = TextEditingController(text: '');
  var obscure = true.obs;
  //TODO: Implement HomeController

  final count = 0.obs;
  LocaleLanguage lang = LocaleLanguage();
  @override
  void onInit() async {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void login(user, pass) async {
    GlobalVariable.loadingPopUp(LocaleKeys.LOADING);
    var data = await ApiHelper().login(user, pass);
    print(data);
    Get.back();
    if (data['status'] == 'OK') {
      userController.clear();
      passController.clear();
      var simpanData = {"idperusahaan": data['data']['idperusahaan'], "idgereja": data['data']['idgereja'], "kodegereja": data['data']['kodegereja'], "idpejabat": data['data']['id'], "gembala": data['data']['gembala'].toString()};
      SharedPreferencesHelper.setDataLogin(simpanData);
      GetToPage.toNamed<HomeController>(Routes.HOME);
    } else {
      GlobalVariable.errorPopUp(LocaleLanguage().tr(LocaleKeys.ERRORUSERPASS));
    }
  }
}
