import 'dart:io';

import 'package:alpha_bpd_apk/app/modules/function/api_helper.dart';
import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:alpha_bpd_apk/app/modules/function/locale_language.dart';
import 'package:alpha_bpd_apk/app/modules/login/controllers/login_controller.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_colors.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/app_fontsize.dart';
import 'package:alpha_bpd_apk/app/modules/static_variable/locale_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginView extends GetView<LoginController> {
  Widget loginContainer(context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Transform.rotate(
          angle: 32,
          child: Container(
            width: GlobalVariable.ratioWidth(context) * 500,
            decoration: BoxDecoration(color: Color.fromRGBO(62, 193, 201, 1), image: DecorationImage(fit: BoxFit.cover, colorFilter: ColorFilter.mode(Color.fromRGBO(62, 193, 201, 1).withOpacity(0.9), BlendMode.srcOver), image: AssetImage("assets/images/login1.jpeg"))),
          ),
        ),
        Transform.rotate(
          angle: 6,
          alignment: Alignment.bottomLeft,
          child: Container(
              width: GlobalVariable.ratioWidth(context) * 500,
              height: GlobalVariable.ratioWidth(context) * 500,
              decoration: BoxDecoration(color: Color.fromRGBO(54, 79, 107, 1), image: DecorationImage(fit: BoxFit.cover, colorFilter: ColorFilter.mode(Color.fromRGBO(54, 79, 107, 1).withOpacity(0.9), BlendMode.srcOver), image: AssetImage("assets/images/login1.jpeg"))),
              child: Stack(
                alignment: Alignment.bottomLeft,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: GlobalVariable.ratioWidth(context) * 20, left: GlobalVariable.ratioWidth(context) * 36),
                    child: Wrap(
                      direction: Axis.horizontal, //Vertical || Horizontal
                      children: <Widget>[
                        Transform.rotate(
                            angle: 38,
                            child: Image.asset(
                              "assets/images/logogbi.png",
                              height: GlobalVariable.ratioWidth(context) * 80,
                            ))
                      ],
                    ),
                  )
                ],
              )),
        ),
        Transform.rotate(
          angle: 1,
          alignment: Alignment.topRight,
          child: Container(
            width: GlobalVariable.ratioWidth(context) * 200,
            decoration: BoxDecoration(color: Color.fromRGBO(252, 82, 133, 1), image: DecorationImage(fit: BoxFit.cover, colorFilter: ColorFilter.mode(Color.fromRGBO(252, 82, 133, 1).withOpacity(0.9), BlendMode.srcOver), image: AssetImage("assets/images/login1.jpeg"))),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          exit(0);
        },
        child: MaterialApp(
            home: Scaffold(
          body: SizedBox(
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  Positioned(height: GlobalVariable.ratioWidth(context) * 240, child: loginContainer(Get.context)),
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 250),
                            Container(
                                width: GlobalVariable.ratioWidth(Get.context!) * 200,
                                child: Text(
                                  LocaleLanguage().tr(LocaleKeys.MARIBERKARYABERSAMAKAMI),
                                  style: TextStyle(fontSize: AppFontSize.xxLarge),
                                )),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            _usernameWidget(),
                            _passwordWidget(),
                            SizedBox(height: GlobalVariable.ratioWidth(Get.context!) * 10),
                            GestureDetector(
                                onTap: () async {
                                  var url = GlobalVariable.apiPath + 'PejabatResetPassword/inputEmail/';
                                  if (!await launchUrl(Uri.parse(url))) {
                                    throw Exception('Could not launch');
                                  }
                                },
                                child: Text(
                                  LocaleLanguage().tr(LocaleKeys.FORGOTPASSWORD) + "?",
                                  style: TextStyle(fontSize: AppFontSize.medium, decoration: TextDecoration.underline),
                                ))
                          ],
                        ),
                      ),
                      _submitButton(),
                    ],
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: Container(width: MediaQuery.of(Get.context!).size.width, child: Text(LocaleLanguage().tr(LocaleKeys.VERSION) + " " + GlobalVariable.version, textAlign: TextAlign.center)),
        )));
  }

  Widget _usernameWidget() {
    return TextField(
      keyboardType: TextInputType.name,
      controller: controller.userController,
      textInputAction: TextInputAction.next,
      style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
        labelText: LocaleLanguage().tr(LocaleKeys.EMAIL) + " / " + LocaleLanguage().tr(LocaleKeys.KODEPEJABAT),
        labelStyle: TextStyle(color: AppColors.borderPejabat, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
      ),
    );
  }

  Widget _passwordWidget() {
    return Obx(() => TextField(
          obscureText: controller.obscure.value,
          keyboardType: TextInputType.visiblePassword,
          controller: controller.passController,
          textInputAction: TextInputAction.done,
          style: TextStyle(fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
          onEditingComplete: () {
            controller.login(controller.userController.value.text, controller.passController.value.text);
          },
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: GlobalVariable.ratioWidth(Get.context!) * 8, vertical: GlobalVariable.ratioWidth(Get.context!) * 12),
            labelText: LocaleLanguage().tr(LocaleKeys.PASSWORD),
            labelStyle: TextStyle(color: AppColors.borderPejabat, fontSize: AppFontSize.medium, fontWeight: FontWeight.w500),
            suffixIcon: Container(
                padding: EdgeInsets.only(top: GlobalVariable.ratioWidth(Get.context!) * 12, bottom: GlobalVariable.ratioWidth(Get.context!) * 12, right: GlobalVariable.ratioWidth(Get.context!) * 8),
                child: GestureDetector(
                    onTap: () {
                      controller.obscure.value = !controller.obscure.value;
                    },
                    child: SvgPicture.asset(controller.obscure.value ? "assets/icon/eye_slash.svg" : "assets/icon/eye.svg"))),
          ),
        ));
  }

  Widget _submitButton() {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () {
          controller.login(controller.userController.text, controller.passController.text);
        },
        child: Stack(alignment: Alignment.centerRight, children: [
          Positioned(
            right: GlobalVariable.ratioWidth(Get.context!) * 10,
            child: SizedBox.fromSize(
              size: Size.square(GlobalVariable.ratioWidth(Get.context!) * 60.0), // button width and height
              child: ClipOval(
                child: Material(
                  color: Color.fromRGBO(252, 228, 138, 1), // button color
                ),
              ),
            ),
          ),
          Container(
            height: GlobalVariable.ratioWidth(Get.context!) * 100,
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: GlobalVariable.ratioWidth(Get.context!) * 38),
            child: Text(LocaleLanguage().tr(LocaleKeys.LOGIN),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: AppFontSize.xxLarge,
                  fontWeight: FontWeight.bold,
                )),
          ),
        ]),
      ),
    );
  }
}
