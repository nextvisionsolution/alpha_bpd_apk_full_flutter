import 'package:alpha_bpd_apk/app/modules/function/global_variable.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

class AppFontSize {
  static double small = GlobalVariable.ratioFontSize(Get.context!) * 12;
  static double medium = GlobalVariable.ratioFontSize(Get.context!) * 14;
  static double large = GlobalVariable.ratioFontSize(Get.context!) * 16;
  static double extraLarge = GlobalVariable.ratioFontSize(Get.context!) * 18;
  static double xLarge = GlobalVariable.ratioFontSize(Get.context!) * 20;
  static double xxLarge = GlobalVariable.ratioFontSize(Get.context!) * 22;
  static double xxxLarge = GlobalVariable.ratioFontSize(Get.context!) * 24;
  static double xxxxLarge = GlobalVariable.ratioFontSize(Get.context!) * 28;
  static double xxxxxLarge = GlobalVariable.ratioFontSize(Get.context!) * 30;
  static double header = GlobalVariable.ratioFontSize(Get.context!) * 20;
}
