import 'package:flutter/cupertino.dart';

class AppColors {
  static const Color divider = Color(0xFFBDBDBD);
  static const Color menuOrange = Color(0xFFEE6352);
  static const Color menuBiru = Color(0xFF08B2E3);
  static const Color menuHijau = Color(0xFF57A773);
  static const Color menuCokelat = Color(0xFF484D6D);
  static const Color menuAbuAbu = Color(0xFF9C9990);
  static const Color borderPejabat = Color(0xFFBEBEBE);
  static const Color backgroundDisabled = Color(0xFFCECECE);
  static const Color border = Color(0xFFD9D9D9);
  static const Color goldColor = Color(0xFFCC992C);
  static const Color separatorColor = Color(0xFFCDCDCD);
  static const Color placeholderColor = Color(0xFFCECECE);
  static const Color backgroundKegiatanColor = Color(0xFF4D4D4D);
}
