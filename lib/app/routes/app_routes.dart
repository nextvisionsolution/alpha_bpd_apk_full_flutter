part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = "/home";
  static const LOGIN = "/login";
  static const SPLASH = "/splash";
  static const PERPULUHAN_IURAN = "/perpuluhan_iuran";
  static const KEGIATAN = "/kegiatan";
  static const PROFIL = "/profil";
  static const BPD = "/bpd";
  static const SURAT = "/surat";
  static const MATERI = "/materi";
  static const SETTING = "/setting";
}

// abstract class _Paths {
//   _Paths._();
//   static const HOME = '/home';
// }
