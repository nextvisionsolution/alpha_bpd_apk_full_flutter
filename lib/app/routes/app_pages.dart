import 'package:alpha_bpd_apk/app/modules/bpd/binding/bpd_binding.dart';
import 'package:alpha_bpd_apk/app/modules/bpd/view/bpd_view.dart';
import 'package:alpha_bpd_apk/app/modules/kegiatan/binding/kegiatan_binding.dart';
import 'package:alpha_bpd_apk/app/modules/kegiatan/view/kegiatan_view.dart';
import 'package:alpha_bpd_apk/app/modules/login/binding/login_binding.dart';
import 'package:alpha_bpd_apk/app/modules/login/view/login_view.dart';
import 'package:alpha_bpd_apk/app/modules/materi/binding/materi_binding.dart';
import 'package:alpha_bpd_apk/app/modules/materi/view/materi_view.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/binding/perpuluhan_iuran_binding.dart';
import 'package:alpha_bpd_apk/app/modules/perpuluhan_iuran/view/perpuluhan_iuran_view.dart';
import 'package:alpha_bpd_apk/app/modules/profil/binding/profil_binding.dart';
import 'package:alpha_bpd_apk/app/modules/profil/view/profil_view.dart';
import 'package:alpha_bpd_apk/app/modules/setting/binding/setting_binding.dart';
import 'package:alpha_bpd_apk/app/modules/setting/view/setting_view.dart';
import 'package:alpha_bpd_apk/app/modules/splash/binding/splash_binding.dart';
import 'package:alpha_bpd_apk/app/modules/splash/view/splash_view.dart';
import 'package:alpha_bpd_apk/app/modules/surat/binding/surat_binding.dart';
import 'package:alpha_bpd_apk/app/modules/surat/view/surat_view.dart';
import 'package:get/get.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(name: Routes.LOGIN, page: () => LoginView(), binding: LoginBinding(), transition: Transition.fadeIn, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.SPLASH, page: () => SplashView(), binding: SplashBinding(), transition: Transition.fadeIn, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.HOME, page: () => HomeView(), binding: HomeBinding(), transition: Transition.cupertino, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.PERPULUHAN_IURAN, page: () => PerpuluhanIuranView(), binding: PerpuluhanIuranBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.KEGIATAN, page: () => KegiatanView(), binding: KegiatanBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.PROFIL, page: () => ProfilView(), binding: ProfilBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.BPD, page: () => BPDView(), binding: BPDBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.SURAT, page: () => SuratView(), binding: SuratBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.MATERI, page: () => MateriView(), binding: MateriBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
    GetPage(name: Routes.SETTING, page: () => SettingView(), binding: SettingBinding(), transition: Transition.rightToLeftWithFade, transitionDuration: Duration(milliseconds: 500)),
  ];
}
